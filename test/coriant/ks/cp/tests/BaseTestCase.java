/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.tests;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.*;
import junit.framework.AssertionFailedError;

/**
 * Base test case that contains support for reporting assertion errors on the test harness thread
 * when these errors are detected in alternative threads.
 */
public abstract class BaseTestCase 
{
	/**
	 * Holds the first failure reported by any thread that participates in the test case 
	 */
	private final AtomicReference<Throwable> threadFailure = new AtomicReference<Throwable>(null);
	
	/**
     * Records an exception so that it can be re-thrown later in the test
     * harness thread, triggering a test case failure.  Only the first
     * failure is recorded; subsequent calls to this method from within
     * the same test have no effect.
     * 
	 * @param t The failure to be recorded
	 */
	public final void threadRecordFailure(Throwable t)
	{
		threadFailure.compareAndSet(null, t);
	}
	
	/**
     * Collects any test case failure reported by any thread, by re-throwing, 
     * in the test harness thread, any exception recorded earlier by threadRecordFailure.
     */
    public final void threadFailureCollect() throws Exception 
    {
        Throwable t = threadFailure.getAndSet(null);
        if (t != null) {
            if (t instanceof Error)
                throw (Error) t;
            else if (t instanceof RuntimeException)
                throw (RuntimeException) t;
            else if (t instanceof Exception)
                throw (Exception) t;
        }
    }

    /**
     * Just like fail(reason), but additionally recording (using
     * threadRecordFailure) any AssertionFailedError thrown, so that
     * the current test case will fail.
     */
    public final void threadFail(String reason) 
    {
    	try {
    		fail(reason);
    	} catch(AssertionError ae) {
    		threadRecordFailure(ae);
    		throw ae;
    	}
    }

    /**
     * Just like assertTrue(b), but additionally recording (using
     * threadRecordFailure) any AssertionFailedError thrown, so that
     * the current test case will fail.
     */
    public final void threadAssertTrue(boolean b) 
    {
        try {
            assertTrue(b);
    	} catch(AssertionError ae) {
    		threadRecordFailure(ae);
    		throw ae;
    	}
    }

    /**
     * Just like assertFalse(b), but additionally recording (using
     * threadRecordFailure) any AssertionFailedError thrown, so that
     * the current test case will fail.
     */
    public final void threadAssertFalse(boolean b) 
    {
        try {
            assertFalse(b);
    	} catch(AssertionError ae) {
    		threadRecordFailure(ae);
    		throw ae;
    	}
    }

    /**
     * Just like assertNull(x), but additionally recording (using
     * threadRecordFailure) any AssertionFailedError thrown, so that
     * the current test case will fail.
     */
    public final void threadAssertNull(Object x) 
    {
        try {
            assertNull(x);
    	} catch(AssertionError ae) {
    		threadRecordFailure(ae);
    		throw ae;
    	}
    }

    /**
     * Just like assertEquals(x, y), but additionally recording (using
     * threadRecordFailure) any AssertionFailedError thrown, so that
     * the current test case will fail.
     */
    public final void threadAssertEquals(long x, long y) 
    {
        try {
            assertEquals(x, y);
    	} catch(AssertionError ae) {
    		threadRecordFailure(ae);
    		throw ae;
    	}
    }

    /**
     * Just like assertEquals(x, y), but additionally recording (using
     * threadRecordFailure) any AssertionFailedError thrown, so that
     * the current test case will fail.
     */
    public final void threadAssertEquals(Object x, Object y) 
    {
        try {
            assertEquals(x, y);
    	} catch(AssertionError ae) {
    		threadRecordFailure(ae);
    		throw ae;
        } catch (Throwable t) {
            threadUnexpectedException(t);
        }
    }

    /**
     * Just like assertSame(x, y), but additionally recording (using
     * threadRecordFailure) any AssertionFailedError thrown, so that
     * the current test case will fail.
     */
    public final void threadAssertSame(Object x, Object y) 
    {
        try {
            assertSame(x, y);
    	} catch(AssertionError ae) {
    		threadRecordFailure(ae);
    		throw ae;
    	}
    }
    
    /**
     * Records the given exception using {@link #threadRecordFailure},
     * then re-throws the exception, wrapping it in an
     * AssertionFailedError if necessary.
     */
    public void threadUnexpectedException(Throwable t) 
    {
        threadRecordFailure(t);
        t.printStackTrace();
        if (t instanceof RuntimeException)
            throw (RuntimeException) t;
        else if (t instanceof Error)
            throw (Error) t;
        else {
            AssertionFailedError afe =
                new AssertionFailedError("unexpected exception: " + t);
            afe.initCause(t);
            throw afe;
        }
    }
}
