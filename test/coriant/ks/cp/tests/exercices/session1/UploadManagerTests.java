/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.tests.exercices.session1;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.*;

import coriant.ks.cp.exercises.session1.solved.UploadManager;
import coriant.ks.cp.tests.BaseTestCase;

/**
 * Class that implements the {@link UploadManager} regular operation test case.
 * 
 * The range of performed tests is limited by the restrictions imposed on the 
 * {@link UploadManager} public interface. More thorough testing would require
 * changes in the tested type's public interface, changes that would render 
 * the use of the simple thread-safety mechanisms inappropriate. 
 *  
 * @author Paulo Pereira
 */
public final class UploadManagerTests extends BaseTestCase {

	/**
	 * The maximum number of simultaneous upload requests that are issued in each test case
	 */
	private static final int REQUEST_COUNT = 20;

	/**
	 * The {@link UploadManager} instance
	 */
	private final UploadManager manager = UploadManager.getInstance();
	
	/** 
	 * The array holding the status of upload requests 
	 */
	private final Request[] requests = new Request[REQUEST_COUNT];

	/**
	 * Internal class used to ensure operation visibility
	 *
	 */
	private static class Request {
		public volatile boolean _completed = false;
	}

	/**
	 * Sets up the test case
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
		// Initialize the array holding pending requests' information 
		for(int id = 0; id < requests.length; ++id)
			requests[id] = new Request();
	}

	/**
	 * Performs clean up after test case completion 
	 * 
	 * @throws Exception If an exception occurred in one of the test case's threads
	 */
	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	/**
	 * Tests normal completion of simultaneous upload requests where there is
	 * no submission of repeated neIds.  
	 */
	@Test
	public void testUploadsNoRequestRepetition()
	{
		// Submit requests
		for(int i = 0; i < requests.length; ++i)
		{
			final int currNeId = i % 2 == 0 ? i : requests.length - i; 
			manager.performUpload(currNeId, new UploadManager.UploadCompletedListener() {
				@Override
				public void notifyUploadCompleted(int neId) 
				{
					threadAssertFalse(requests[neId]._completed);
					requests[neId]._completed = true;
				}
			});
		}
		
		// Wait for completion
		// The time we wait is coupled with the expected duration of each upload
		// Ideally we should synchronize with threads' completion, but they are 
		// not accessible
		try { Thread.sleep(60*1000); }
		catch(InterruptedException unexpected) 
		{ 
			throw new UnknownError("Unexpected InterruptedException in test harness thread"); 
		}

		// Check if all requests are completed  
		for(Request request : requests)
			assertTrue(request._completed);
	}
	
	/**
	 * Tests normal completion of simultaneous upload requests where there is
	 * repeated submission of neIds while previous requests have not yet been completed.
	 */
	@Test
	public void testUploadsWithPendingRequestRepetition()
	{
		// Submit requests
		for(int i = 0; i < requests.length * 2; ++i)
		{
			final int currNeId = i % requests.length;
			manager.performUpload(currNeId, new UploadManager.UploadCompletedListener() {
				@Override
				public void notifyUploadCompleted(int neId) 
				{
					threadAssertFalse(requests[neId]._completed);
					requests[neId]._completed = true;
				}
			});
		}
		
		// Wait for completion
		// The time we wait is coupled with the expected duration of each upload
		// Ideally we should synchronize with threads' completion, but they are 
		// not accessible
		try { Thread.sleep(60*1000); }
		catch(InterruptedException unexpected) 
		{ 
			throw new UnknownError("Unexpected InterruptedException in test harness thread"); 
		}

		// Check if all requests are completed
		for(Request request : requests)
			assertTrue(request._completed);
	}
	
	/**
	 * Tests normal completion of simultaneous upload requests where there is
	 * repeated submission of neIds after previous requests have been completed.
	 */
	@Test
	public void testUploadsWithCompletedRequestRepetition()
	{
		// Submit requests
		for(int id = 0; id < requests.length; ++id)
		{
			manager.performUpload(id, new UploadManager.UploadCompletedListener() {
				@Override
				public void notifyUploadCompleted(int neId) 
				{
					threadAssertFalse(requests[neId]._completed);
					requests[neId]._completed = true;
				}
			});
		}
		
		// Wait for completion
		// The time we wait is coupled with the expected duration of each upload
		// Ideally we should synchronize with threads' completion, but they are 
		// not accessible
		try { Thread.sleep(60*1000); }
		catch(InterruptedException unexpected) 
		{ 
			throw new UnknownError("Unexpected InterruptedException in test harness thread"); 
		}

		// Check if all requests are completed  
		for(Request request : requests)
			assertTrue(request._completed);
		
		// Do it again! This time toggling completion and asserting it was alreadyå true
		for(int id = 0; id < requests.length; ++id)
		{
			manager.performUpload(id, new UploadManager.UploadCompletedListener() {
				@Override
				public void notifyUploadCompleted(int neId) 
				{
					threadAssertTrue(requests[neId]._completed);
					requests[neId]._completed = false;
				}
			});
		}

		// Again, wait for completion
		try { Thread.sleep(60*1000); }
		catch(InterruptedException unexpected) 
		{ 
			throw new UnknownError("Unexpected InterruptedException in test harness thread"); 
		}

		// Check if all requests were completed (the _completed flag was toggled) 
		for(Request request : requests)
			assertFalse(request._completed);
	}
	
}
