/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.tests.exercices.session1;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import coriant.ks.cp.exercises.session1.solved.SimpleCache;
import coriant.ks.cp.tests.BaseTestCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Class that implements the {@link SimpleCache} regular operation test case.
 *   
 * The range of performed tests is limited by the restrictions imposed on the 
 * {@link SimpleCache} public interface. More thorough testing would require
 * changes in the tested type's public interface, changes that would render 
 * the use of the simple thread-safety mechanisms inappropriate.
 *  
 * @author Paulo Pereira
 */
public final class SimpleCacheTests extends BaseTestCase {

	/**
	 * The factorial calculus function
	 * 
	 * @param value the input value 
	 * @return the value's factorial
	 */
	private static int factorial(int value)
	{
		assertTrue(value >= 0);
		int result = 1;
		while(value > 0)
			result *= value--;
		return Integer.valueOf(result);
	}

	/**
	 * A really slow factorial implementation. Given the time spent in each computation, it is worthwhile to cache 
	 * this CPU bound operation's results. 
	 */
	private final SimpleCache.Func<Integer,Integer> slowFactorial = new SimpleCache.Func<Integer,Integer>() {
		// A really slow factorial computation =)
		private static final int STEP_DELAY = 1;
		public Integer call(Integer key)
		{
			final int value = key.intValue();
			try { Thread.sleep(STEP_DELAY * value); }
			catch(InterruptedException impossible) { /* does not occur */ }
			
			return Integer.valueOf(factorial(value));
		}
	};
	
	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	/**
	 * Check if the cache is being effective in amortizing the costs of the 
	 * slow computation.  
	 */
	@Test
	public void testCacheOperationSingleEntrySingleThread()
	{
		final int REPETITION_COUNT = 10;
		final int FACTORIAL_TO_COMPUTE = 30;
		
		// Measure time without the use of a cache
		long startTime = System.currentTimeMillis();
		int value = slowFactorial.call(Integer.valueOf(FACTORIAL_TO_COMPUTE));
		for(int i=0;i<REPETITION_COUNT-1;++i)
			slowFactorial.call(Integer.valueOf(FACTORIAL_TO_COMPUTE));
		long ellapsedTimeNoCache = System.currentTimeMillis() - startTime;
		
		// Measure time while using the cache
		final SimpleCache<Integer, Integer> cache = new SimpleCache<>(slowFactorial);
		startTime = System.currentTimeMillis();
		for(int i=0;i<REPETITION_COUNT;++i)
			// Ensure the value is correct in each call
			assertEquals(value, cache.get(Integer.valueOf(FACTORIAL_TO_COMPUTE)).intValue());
		final long ellapsedTimeWithCache = System.currentTimeMillis() - startTime;
		
		// A significant performance improvement is expected. Assert it!
		assertTrue(ellapsedTimeNoCache > ellapsedTimeWithCache);
		System.out.println(String.format("testCacheOperationSingleEntrySingleThread: (no cache) %1$d > (cache) %2$d", 
				ellapsedTimeNoCache, ellapsedTimeWithCache));
	}
	
	/**
	 * Helper method that launches THREAD_COUNT threads and waits for their completion, 
	 * returning the elapsed time in milliseconds
	 *  
	 * @param work The threads' code
	 * @param threadCount the number of threads to start
	 * @param timeout the time to wait for completion
	 * @return The elapsed time in milliseconds
	 */
	private long performTestWithMultipleThreadsHelper(Runnable work, int threadCount, long timeout) throws InterruptedException
	{
		final Thread[] participants = new Thread[threadCount];

		// Start threads
		final long startTime = System.currentTimeMillis();
		for(int threadId=0; threadId<participants.length; ++threadId)
			(participants[threadId] = new Thread(work)).start();
		
		// Wait for completion
		for(Thread participant : participants)
		{
			participant.join(timeout);
			assertFalse(participant.isAlive());
		}
		
		// return elapsed time
		return System.currentTimeMillis() - startTime; 
	}
	
	/**
	 * Test cache correctness and effectiveness when accessed by multiple threads 
	 */
	@Test
	public void testCacheOperation()
	{
		final int FACTORIAL_RANGE = 20;

		// Compute correct values
		final int[] factorialValues = new int[FACTORIAL_RANGE];
		for(int i=0; i<factorialValues.length; ++i)
			factorialValues[i] = factorial(i);
		
		final int FACTORIAL_CALL_COUNT = 5000;
		final int COMPLETION_TIMEOUT = 2*60*1000; // related to the previous value
		final int SEED = 5;
		final int THREAD_COUNT = 4;

		try {
			// Start threads that will require computed values and will not share a cache
			final long noCacheTotalTime = performTestWithMultipleThreadsHelper(new Runnable() {
				@Override
				public void run() 
				{
					final Random generator = new Random(SEED); 
					for(int i=0; i<FACTORIAL_CALL_COUNT; ++i)
					{
						// Randomize input value in the range [0..FACTORIAL_RANGE]
						int inputValue = generator.nextInt(FACTORIAL_RANGE);
						// Assert factorial result's correctness for the current input value
						assertEquals(factorialValues[inputValue], slowFactorial.call(inputValue).intValue());
					}
				}
			}, THREAD_COUNT, COMPLETION_TIMEOUT);
			
			// Lets do it again, this time using the cache implementation
			final SimpleCache<Integer, Integer> cache = new SimpleCache<>(slowFactorial);
			final long withCacheTotalTime = performTestWithMultipleThreadsHelper(new Runnable() {
				@Override
				public void run() 
				{
					final Random generator = new Random(SEED); 
					for(int i=0; i<FACTORIAL_CALL_COUNT; ++i)
					{
						// Randomize input value in the range [0..FACTORIAL_RANGE]
						int inputValue = generator.nextInt(FACTORIAL_RANGE);
						// Assert factorial result's correctness for the current input value
						assertEquals(factorialValues[inputValue], cache.get(inputValue).intValue());
					}
				}
			}, THREAD_COUNT, COMPLETION_TIMEOUT);

			
			// A significant performance improvement is expected. Assert it!
			assertTrue( noCacheTotalTime > withCacheTotalTime);
			System.out.println(String.format("testCacheOperation: (no cache) %1$d > (cache) %2$d", noCacheTotalTime, withCacheTotalTime));
		}
		catch(InterruptedException unexpected)
		{
			throw new UnknownError("Unexpected InterruptedException in test harness thread");
		}
	}
}
