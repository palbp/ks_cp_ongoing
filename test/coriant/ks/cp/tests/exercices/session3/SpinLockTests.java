/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.tests.exercices.session3;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import coriant.ks.cp.exercises.session3.solved.SpinLock;
import coriant.ks.cp.tests.BaseTestCase;

/**
 * Class that implements the {@link SpinLockTests} test case.
 */
public final class SpinLockTests  extends BaseTestCase {

	/**
	 * Shared variable used to assert that the SpinLock implementation
	 * is ensuring atomicity.
	 */
	private volatile int _sharedCounter;

	/**
	 * Number of increments that each participant thread performs on the shared state.
	 */
	static final int INCREMENT_COUNT = 1_000_000;
	
	/**
	 * Sets up the test case
	 */
	@Before
	public void setUp() 
	{
		_sharedCounter = 0;
	}

	/**
	 * Performs clean up after test case completion 
	 * 
	 * @throws Exception If an exception occurred in one of the test case's threads
	 */
	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	/**
	 * Tests if atomicity requirements manifest themselves with the test's parameters.
	 */
	@Test
	public void testAtomicityGarantiesNecessity() throws InterruptedException
	{
		final int THREAD_COUNT = 5;
		final Thread[] participants = new Thread[THREAD_COUNT];
		
		// Assert that concurrent accesses require synchronization  
		for(int i = 0; i < participants.length; ++i)
			(participants[i] = new Thread() {
				@Override
				public void run()
				{
					for(int i = 0; i < INCREMENT_COUNT; ++i)
						_sharedCounter += 1;
				}
			}).start();
		
		// Wait for termination
		for(Thread t : participants)
			t.join();
		
		System.out.println(_sharedCounter);
		// A race must have occurred...
		assertFalse(INCREMENT_COUNT*THREAD_COUNT == _sharedCounter);
	}
	
	/**
	 * Tests if the {@link SpinLock} is ensuring atomicity.
	 */
	@Test
	public void testAtomicityGaranties() throws InterruptedException
	{
		final int THREAD_COUNT = 5;
		final Thread[] participants = new Thread[THREAD_COUNT];

		// Assert that concurrent accesses are synchronized
		final SpinLock sync = new SpinLock();
		for(int i = 0; i < participants.length; ++i)
			(participants[i] = new Thread() {
				@Override
				public void run()
				{
					for(int i = 0; i < INCREMENT_COUNT; ++i)
					{
						try {
							sync.lock();
							_sharedCounter += 1;
						}
						finally {
							sync.unlock();
						}
					}
				}
			}).start();
		
		// Wait for termination
		for(Thread t : participants)
			t.join();
		
		System.out.println(_sharedCounter);
		
		// Assert that there were no races
		assertEquals(INCREMENT_COUNT*THREAD_COUNT, _sharedCounter);
	}
}
