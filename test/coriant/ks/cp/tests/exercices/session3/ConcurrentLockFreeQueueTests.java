/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.tests.exercices.session3;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import coriant.ks.cp.exercises.session3.solved.ConcurrentLockFreeQueue;
import coriant.ks.cp.tests.BaseTestCase;

/**
 * Class that implements the {@link ConcurrentLockFreeQueueTests} test case.
 */
public final class ConcurrentLockFreeQueueTests  extends BaseTestCase {
	
	/**
	 * Holds the shared queue instance.
	 */
	private volatile ConcurrentLockFreeQueue<Integer> _queue;
	
	/**
	 * The number of elements in the input data.
	 */
	private static final int INPUT_DATA_SIZE = 1 << 12;
	
	/**
	 * Holds the tests' input data.
	 */
	private volatile List<Integer> _inputData;
	
	/**
	 * Holds the tests' output data.
	 */
	private volatile List<Integer> _outputData;
	
	/**
	 * Holds the count of consumed items.
	 */
	private volatile AtomicInteger _consumedItemCount;
	
	/**
	 * Represents the consumer threads that are used in the test case.
	 */
	private class ConsumerThread extends Thread 
	{
		@Override
		public void run()
		{
			ArrayList<Integer> threadLocalConsumedItems = new ArrayList<Integer>();
			while(_consumedItemCount.get() != INPUT_DATA_SIZE)
			{
				if(!_queue.isEmpty())
				{
					Integer elem = _queue.take();
					if(elem != null)
					{
						threadLocalConsumedItems.add(elem);
						_consumedItemCount.incrementAndGet();
					}
				}
			}
			
			// Copy thread local data to shared state
			_outputData.addAll(threadLocalConsumedItems);
		}
	}
	
	/**
	 * Represents the producer threads that are used in the test case.
	 */
	private class ProducerThread extends Thread 
	{
		@Override
		public void run()
		{
			for(int item : _inputData)
				_queue.put(item);
		}
	}
	
	/**
	 * Sets up the test case.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
		// The synchronizer instance to test
		_queue = new ConcurrentLockFreeQueue<>();
		
		// Initialize test input data 
		List<Integer> data = new ArrayList<>(INPUT_DATA_SIZE);
		for(int i = 0; i < INPUT_DATA_SIZE; ++i)
			data.add(i);
		_inputData = Collections.unmodifiableList(data);
		
		// Initialize output data container (shared state)
		_outputData = Collections.synchronizedList(new ArrayList<Integer>(INPUT_DATA_SIZE));
		_consumedItemCount = new AtomicInteger(0);
	}

	/**
	 * Performs clean up after test case completion 
	 * 
	 * @throws Exception If an exception occurred in one of the test case's threads
	 */
	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	/**
	 * Tests if the queue's FIFO discipline is enforced when there are no concurrent 
	 * accesses.
	 */
	@Test
	public void testFifoDisciplineNoConcurrency()
	{
		final int ITEM_COUNT = 1 << 12;
		
		// Push items 
		for(int item = 0; item < ITEM_COUNT; ++item)
			_queue.put(item);
		
		// Pop items
		int currentCount = 0;
		while(!_queue.isEmpty() && currentCount != ITEM_COUNT)
		{
			int currValue = _queue.take();
			// Assert retrieval order correctness
			assertEquals(currentCount++, currValue);
		}
		
		// Stack must be empty
		assertTrue(_queue.isEmpty());
	}

	/**
	 * Tests if the queue's FIFO discipline is enforced when there are concurrent 
	 * accesses.
	 */
	@Test
	public void testSingleProducerSingleConsumer()
	{
		Thread producer = new ProducerThread();
		Thread consumer = new ConsumerThread();

		// Start participant threads
		producer.start();
		consumer.start();
		
		// Wait for termination
		try {
			producer.join();
			consumer.join();
		} catch (InterruptedException unexpected) 
		{
			UnknownError error = new UnknownError();
			error.initCause(unexpected);
			throw error;
		}
		
		// Verify results
		assertArrayEquals(_inputData.toArray(), _outputData.toArray());
		// Stack must be empty
		assertTrue(_queue.isEmpty());
	}
	
	/**
	 * Tests the stack in a scenario of single producer and multiple consumers.
	 */
	@Test
	public void testSingleProducerMultipleConsumers()
	{
		Thread producer = new ProducerThread();
		
		final int CONSUMER_COUNT = 4;
		Thread[] consumers = new Thread[CONSUMER_COUNT];
		for(int i = 0; i < consumers.length; ++i)
			consumers[i] = new ConsumerThread();

		// Start participant threads
		producer.start();
		for(Thread consumer : consumers)
			consumer.start();
		
		// Wait for termination
		try {
			producer.join();
			for(Thread consumer : consumers)
				consumer.join();
		} catch (InterruptedException unexpected) 
		{
			UnknownError error = new UnknownError();
			error.initCause(unexpected);
			throw error;
		}
		
		// Verify results
		Collections.sort(_outputData);
		assertArrayEquals(_inputData.toArray(), _outputData.toArray());
		// Stack must be empty
		assertTrue(_queue.isEmpty());
	}
}
