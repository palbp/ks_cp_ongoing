package coriant.ks.cp.tests.exercices.session2;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import coriant.ks.cp.tests.BaseTestCase;

import coriant.ks.cp.exercises.session2.solved.*;
//import coriant.ks.cp.exercises.session2.*;

/**
 * Class that implements the {@link UploadExecutionService} test case.
 *  
 * @author Paulo Pereira
 */
public final class UploadManagerTests extends BaseTestCase {

	private static final int MAGIC_RESULT = 42;
	private static final int MAGIC_ID = 12;
	
	private static final int WORK_TIME = 500; 
	
	/**
	 * Holds the execution service instance to use.
	 */
	private volatile UploadManager<Integer> _manager;
	
	private static final int MAX_UPLOADS = 2;
	
	/**
	 * Sets up the test case
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
		_manager = new UploadManager<Integer>(MAX_UPLOADS, new UploadExecutionService<Integer>(), new UploadManager.UploadWorker<Integer>() {
			@Override
			public Integer doUpload(int neId, UploadType type) throws Exception
			{
				try { Thread.sleep(WORK_TIME); }
				catch(InterruptedException impossible) { } // does not occur
				return MAGIC_RESULT;
			}
		});
	}

	/**
	 * Performs clean up after test case completion 
	 * 
	 * @throws Exception If an exception occurred in one of the test case's threads
	 */
	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	/**
	 * Tests rejection of redundant upload requests.  
	 */
	@Test(expected = IllegalStateException.class)
	public void testRedundantRejection() throws Exception
	{
		UploadTask<Integer> task15 = null;
		try {
			task15 = _manager.performUpload(MAGIC_ID, UploadType.INTERVAL_15_MIN);
			_manager.performUpload(MAGIC_ID, UploadType.INTERVAL_15_MIN);
		}
		finally {
			task15.getResult();
		}
	}

	/**
	 * Tests rejection of redundant throttled upload requests.  
	 */
	@Test(expected = IllegalStateException.class)
	public void testRedundantThrottledRejection() throws Exception
	{
		UploadTask<Integer> task15 = null, task24 = null;
		try {
			task15 = _manager.performThrottledUpload(MAGIC_ID, UploadType.INTERVAL_15_MIN);
			task24 = _manager.performThrottledUpload(MAGIC_ID, UploadType.INTERVAL_24_HRS);
			_manager.performUpload(MAGIC_ID, UploadType.INTERVAL_15_MIN);
		}
		finally {
			task15.getResult();
			task24.getResult();
		}
	}
	
	/**
	 * Tests manager's throttle.  
	 */
	@Test
	public void testThrottled() throws Exception
	{
		UploadTask<Integer> task15 = _manager.performThrottledUpload(MAGIC_ID, UploadType.INTERVAL_15_MIN);
		UploadTask<Integer> task24 = _manager.performThrottledUpload(MAGIC_ID+1, UploadType.INTERVAL_24_HRS);
		UploadTask<Integer> throttled = _manager.performThrottledUpload(MAGIC_ID+2, UploadType.INTERVAL_15_MIN);
		Thread.sleep(WORK_TIME/10);
		// Check status of submitted tasks
		assertEquals(UploadTask.Status.INPROGRESS, task15.getStatus());
		assertEquals(UploadTask.Status.INPROGRESS, task24.getStatus());
		assertEquals(UploadTask.Status.PENDING, throttled.getStatus());
		// Wait for results and check them
		assertTrue(task15.getResult() == MAGIC_RESULT);
		assertTrue(task24.getResult() == MAGIC_RESULT);
		assertTrue(throttled.getResult() == MAGIC_RESULT);
	}
	
	/**
	 * Tests pending throttled request promotion.  
	 */
	@Test
	public void testPendingThrottledPromotion() throws Exception
	{
		UploadTask<Integer> task15 = _manager.performThrottledUpload(MAGIC_ID, UploadType.INTERVAL_15_MIN);
		UploadTask<Integer> task24 = _manager.performThrottledUpload(MAGIC_ID+1, UploadType.INTERVAL_24_HRS);
		UploadTask<Integer> throttled = _manager.performThrottledUpload(MAGIC_ID+2, UploadType.INTERVAL_15_MIN);
		Thread.sleep(WORK_TIME/10);
		// Check status of submitted tasks
		assertEquals(UploadTask.Status.INPROGRESS, task15.getStatus());
		assertEquals(UploadTask.Status.INPROGRESS, task24.getStatus());
		assertEquals(UploadTask.Status.PENDING, throttled.getStatus());
		// Promote pending throttled
		UploadTask<Integer> promoted = _manager.performUpload(MAGIC_ID+2, UploadType.INTERVAL_15_MIN);
		assertSame(throttled, promoted);
		Thread.sleep(WORK_TIME/10);
		assertEquals(UploadTask.Status.INPROGRESS, throttled.getStatus());
		// Wait for results and check them
		assertTrue(task15.getResult() == MAGIC_RESULT);
		assertTrue(task24.getResult() == MAGIC_RESULT);
		assertTrue(throttled.getResult() == MAGIC_RESULT);
	}
}
