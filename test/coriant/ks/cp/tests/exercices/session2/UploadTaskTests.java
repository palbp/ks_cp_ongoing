package coriant.ks.cp.tests.exercices.session2;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import coriant.ks.cp.tests.BaseTestCase;
import coriant.ks.cp.utils.Functions;

//import coriant.ks.cp.exercises.session2.*;
import coriant.ks.cp.exercises.session2.solved.*;

/**
 * Class that implements the {@link UploadTask} test case.
 *  
 * @author Paulo Pereira
 */
public final class UploadTaskTests extends BaseTestCase {

	private static final int MAGIC_RESULT = 42;
	private static final int MAGIC_ID = 12;
	
	private static class SharedFlag { public volatile boolean _flag = false; }
	
	/**
	 * Task action that succeeds.
	 */
	private final Callable<Integer> _taskActionOK = new Callable<Integer>() {
		@Override
		public Integer call() throws Exception 
		{
			try { Thread.sleep(200); }
			catch(InterruptedException impossible) { } // does not occur
			return MAGIC_RESULT;
		}
	};
	
	/**
	 * Task action that produces an error. 
	 */
	private final Callable<Integer> _taskActionError = new Callable<Integer>() {
		@Override
		public Integer call() throws Exception 
		{
			try { Thread.sleep(200); }
			catch(InterruptedException impossible) { } // does not occur
			throw new IOException("Simulated I/O error");
		}
	};
	
	/**
	 * Sets up the test case
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
	}

	/**
	 * Performs clean up after test case completion 
	 * 
	 * @throws Exception If an exception occurred in one of the test case's threads
	 */
	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	/**
	 * Tests normal completion of an UploadTask through in-line execution. The task's action 
	 * does not produce an error.  
	 */
	@Test
	public void testInlineUploadTaskCompletionOK() throws Exception
	{
		final UploadTask<Integer> task = new UploadTask<Integer>(MAGIC_ID, UploadType.INTERVAL_15_MIN, _taskActionOK);
		assertEquals(UploadTask.Status.PENDING, task.getStatus());
		task.run();
		assertEquals(UploadTask.Status.COMPLETED, task.getStatus());
		assertTrue(task.getResult().intValue() == MAGIC_RESULT);
	}
	
	/**
	 * Tests normal completion of an UploadTask through in-line execution. The task's action 
	 * produces an error.  
	 */
	@Test(expected = IOException.class)
	public void testInlineUploadTaskCompletionError() throws Throwable
	{
		final UploadTask<Integer> task = new UploadTask<Integer>(MAGIC_ID, UploadType.INTERVAL_15_MIN, _taskActionError);
		assertEquals(UploadTask.Status.PENDING, task.getStatus());
		task.run();
		assertEquals(UploadTask.Status.COMPLETED, task.getStatus());
		try { task.getResult(); }
		catch(ExecutionException actionError) { throw actionError.getCause(); }
	}
	
	/**
	 * Tests a continuations of an UploadTask through in-line execution.
	 */
	@Test
	public void testInlineUploadTaskContinuation() throws Exception
	{
		final SharedFlag whenDone = new SharedFlag(), continueWith = new SharedFlag();
		
		final UploadTask<Integer> task = new UploadTask<Integer>(MAGIC_ID, UploadType.INTERVAL_15_MIN, _taskActionOK)
				.whenDone(new Functions.Action<UploadTask<Integer>>() {
					@Override
					public void call(UploadTask<Integer> completedTask) 
					{
						assertFalse(whenDone._flag || continueWith._flag);
						whenDone._flag = true;
					}
				}).continueWith(new Functions.Action<UploadTask<Integer>>() {
					@Override
					public void call(UploadTask<Integer> continuedTask) 
					{
						assertTrue(whenDone._flag);
						assertFalse(continueWith._flag);
						assertEquals(UploadTask.Status.COMPLETED, continuedTask.getStatus());
						continueWith._flag = true;
					}
		});
		
		assertEquals(UploadTask.Status.PENDING, task.getStatus());
		task.run();
		assertEquals(UploadTask.Status.COMPLETED, task.getStatus());
		assertTrue(task.getResult().intValue() == MAGIC_RESULT);
		assertTrue(whenDone._flag && continueWith._flag);
	}
	
	/**
	 * Tests continuations of an UploadTask through execution in alternative thread.
	 */
	@Test
	public void testUploadTaskContinuation() throws Exception
	{
		final SharedFlag whenDone = new SharedFlag(), continueWith = new SharedFlag();
		
		final UploadTask<Integer> task = new UploadTask<Integer>(MAGIC_ID, UploadType.INTERVAL_15_MIN, _taskActionOK)
				.whenDone(new Functions.Action<UploadTask<Integer>>() {
					@Override
					public void call(UploadTask<Integer> completedTask) 
					{
						assertFalse(whenDone._flag || continueWith._flag);
						try { assertTrue(completedTask.getResult().intValue() == MAGIC_RESULT); } 
						catch (Exception e) { assertTrue(false); }
						whenDone._flag = true;
					}
				}).continueWith(new Functions.Action<UploadTask<Integer>>() {
					@Override
					public void call(UploadTask<Integer> continuedTask) 
					{
						assertTrue(whenDone._flag);
						assertFalse(continueWith._flag);
						assertEquals(UploadTask.Status.COMPLETED, continuedTask.getStatus());
						try { assertTrue(continuedTask.getResult().intValue() == MAGIC_RESULT); } 
						catch (Exception e) { assertTrue(false); }
						continueWith._flag = true;
					}
		});
		
		Thread worker = new Thread(task);
		assertEquals(UploadTask.Status.PENDING, task.getStatus());
		worker.start();
		assertTrue(task.getResult().intValue() == MAGIC_RESULT);
		assertEquals(UploadTask.Status.COMPLETED, task.getStatus());
		// Wait for worker termination
		worker.join();
		// Assert continuations execution
		assertTrue(whenDone._flag && continueWith._flag);
	}
}
