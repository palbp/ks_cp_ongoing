package coriant.ks.cp.tests.exercices.session2;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.Callable;

import coriant.ks.cp.tests.BaseTestCase;

import coriant.ks.cp.exercises.session2.solved.*;
//import coriant.ks.cp.exercises.session2.*;

/**
 * Class that implements the {@link UploadExecutionService} test case.
 *  
 * @author Paulo Pereira
 */
public final class UploadExecutionServiceTests extends BaseTestCase {

	private static final int MAGIC_RESULT = 42;
	private static final int MAGIC_ID = 12;
	
	private static final int WORK_TIME = 500; 
	
	/**
	 * Holds the execution service instance to use.
	 */
	private volatile UploadExecutionService<Integer> _executor;
	
	/**
	 * Task action that simulates a successful upload.
	 */
	private final Callable<Integer> _taskActionOK = new Callable<Integer>() {
		@Override
		public Integer call() throws Exception 
		{
			try { Thread.sleep(WORK_TIME); }
			catch(InterruptedException impossible) { } // does not occur
			return MAGIC_RESULT;
		}
	};
	
	
	/**
	 * Sets up the test case
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
		_executor = new UploadExecutionService<>();
	}

	/**
	 * Performs clean up after test case completion 
	 * 
	 * @throws Exception If an exception occurred in one of the test case's threads
	 */
	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	/**
	 * Tests delaying of conflicting uploads.  
	 */
	@Test
	public void testDelayed() throws Exception
	{
		final UploadTask<Integer> task15 = new UploadTask<Integer>(MAGIC_ID, UploadType.INTERVAL_15_MIN, _taskActionOK);
		final UploadTask<Integer> task24 = new UploadTask<Integer>(MAGIC_ID, UploadType.INTERVAL_24_HRS, _taskActionOK);
		
		_executor.put(task15);
		_executor.put(task24);
		
		// Spin wait for the execution start of task15
		while(task15.getStatus() != UploadTask.Status.INPROGRESS);
		// task24 must still be pending because it should be delayed
		assertEquals(UploadTask.Status.PENDING, task24.getStatus());
		// wait for task15 completion
		assertTrue(task15.getResult().intValue() == MAGIC_RESULT);
		Thread.sleep(WORK_TIME/10);
		// task24 should already be in execution...
		assertEquals(UploadTask.Status.INPROGRESS, task24.getStatus());
		// wait for task24 completion
		assertTrue(task24.getResult().intValue() == MAGIC_RESULT);
	}
	
	/**
	 * Tests refusal of conflicting uploads.  
	 */
	@Test
	public void testOffer() throws Exception
	{
		final UploadTask<Integer> task15 = new UploadTask<Integer>(MAGIC_ID, UploadType.INTERVAL_15_MIN, _taskActionOK);
		final UploadTask<Integer> task24 = new UploadTask<Integer>(MAGIC_ID, UploadType.INTERVAL_24_HRS, _taskActionOK);
		
		_executor.put(task15);
		// Offer a conflicting upload and check if it is refused
		assertFalse(_executor.offer(task24));
		// wait for task15 completion
		assertTrue(task15.getResult().intValue() == MAGIC_RESULT);
		// Offer the previously conflicting upload, but not anymore. Check if it is accepted
		assertTrue(_executor.offer(task24));
		// wait for task24 completion
		assertTrue(task24.getResult().intValue() == MAGIC_RESULT);
	}
}
