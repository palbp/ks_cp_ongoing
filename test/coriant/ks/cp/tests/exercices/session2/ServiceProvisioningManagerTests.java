package coriant.ks.cp.tests.exercices.session2;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import coriant.ks.cp.tests.BaseTestCase;

import coriant.ks.cp.exercises.session2.solved.*;
//import coriant.ks.cp.exercises.session2.*;

/**
 * Class that implements the {@link ServiceProvisioningManager} test case.
 *  
 * @author Paulo Pereira
 */
public final class ServiceProvisioningManagerTests extends BaseTestCase {
	
	private static class CompletionFlag { public volatile int _serviceID; }

	/**
	 * Sets up the test case
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
	}

	/**
	 * Performs clean up after test case completion 
	 * 
	 * @throws Exception If an exception occurred in one of the test case's threads
	 */
	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	/**
	 * Tests normal operation of the ServiceProvisioningManager implemented
	 * with the specific notification pattern.  
	 */
	@Test
	public void testNormalOperationSN() throws InterruptedException
	{
		final ServiceProvisioningManager _manager = new ServiceProvisioningManager();
		final int SERVICE_ID = 42;
		
		final CompletionFlag completed = new CompletionFlag();
		
		// Start thread that will signal service provisioning completion
		// Used to simulate delay on notifications received by OLM  
		(new Thread() {
			@Override
			public void run()
			{
				try { Thread.sleep(2000); }
				catch(InterruptedException irrelevant) { }
				completed._serviceID = SERVICE_ID;
				_manager.provisionServiceComplete(completed._serviceID);
			}
		}).start();
		
		// Start thread that will also wait for completion
		Thread participant = new Thread() {
			@Override
			public void run()
			{
				// Block and wait for completion
				try { 
					_manager.provisionService(SERVICE_ID);
					threadAssertTrue(completed._serviceID == SERVICE_ID);
				}
				catch(InterruptedException unexpected) 
				{
					threadFail("Participant thread was interrupted");
				}
			}
		};
		participant.start();
		
		// Block and wait for completion
		_manager.provisionService(SERVICE_ID);
		assertTrue(completed._serviceID == SERVICE_ID);
		// Wait for the termination of the other participant
		participant.join();
	}
	
	/**
	 * Tests normal operation of the ServiceProvisioningManager implemented
	 * with the delegation of execution pattern.  
	 */
	@Test
	public void testNormalOperationDE() throws InterruptedException
	{
		final ServiceProvisioningManagerDE _manager = new ServiceProvisioningManagerDE();
		final int SERVICE_ID = 42;
		
		final CompletionFlag completed = new CompletionFlag();
		
		// Start thread that will signal service provisioning completion
		// Used to simulate delay on notifications received by OLM  
		(new Thread() {
			@Override
			public void run()
			{
				try { Thread.sleep(2000); }
				catch(InterruptedException irrelevant) { }
				completed._serviceID = SERVICE_ID;
				_manager.provisionServiceComplete(completed._serviceID);
			}
		}).start();
		
		// Start thread that will also wait for completion
		Thread participant = new Thread() {
			@Override
			public void run()
			{
				// Block and wait for completion
				try { 
					_manager.provisionService(SERVICE_ID);
					threadAssertTrue(completed._serviceID == SERVICE_ID);
				}
				catch(InterruptedException unexpected) 
				{
					threadFail("Participant thread was interrupted");
				}
			}
		};
		participant.start();
		
		// Block and wait for completion
		_manager.provisionService(SERVICE_ID);
		assertTrue(completed._serviceID == SERVICE_ID);
		// Wait for the termination of the other participant
		participant.join();
	}
}
