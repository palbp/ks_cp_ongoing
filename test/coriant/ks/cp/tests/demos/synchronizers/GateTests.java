package coriant.ks.cp.tests.demos.synchronizers;

import static org.junit.Assert.*;
import coriant.ks.cp.demos.synchronizers.Gate;
import coriant.ks.cp.tests.BaseTestCase;

import org.junit.*;

public class GateTests extends BaseTestCase {
	
	/**
	 * Utility method that starts the given number of participant threads.
	 * 
	 * @param gate The gate instance to be used in the unit test
	 * @param count The number of participant threads
	 * @param startWaitTime The time (in milliseconds) that the test harness thread should 
	 * wait for participants to reach the gate.
	 * @return An array bearing the participant threads
	 * @throws InterruptedException If the calling thread is cancelled
	 */
	private Thread[] startParticipants(final Gate gate, int count, long startWaitTime) throws InterruptedException
	{
		final Thread[] participants = new Thread[count];
		
		for(int i = 0; i<participants.length; ++i)
		{
			final int currIdx = i;
			(participants[i] = new Thread() {
				private final int id = currIdx;
				
				public void run()
				{
					try { gate.await(); }
					catch(InterruptedException expectedOnlyAtEvenIds) 
					{
						if(id%2 != 0)
							threadFail("Participant thread was interrupted");
					}
				}
			}).start();
		}
		
		// Wait for participants to reach the gate
		// Implementation note: We should use explicit synchronization (e.g. CountDownLatch
		// or CyclicBarrier) to ensure that unfortunate timings do not result in test failure. 
		Thread.sleep(startWaitTime);
		
		return participants;
	}
	
	/**
	 * Utility method that blocks the calling thread until all participant threads terminate,
	 * or the specified timeout elapses.
	 * @param participants The threads to wait for 
	 * @param terminationWaitTime The termination timeout (in milliseconds)
	 * @throws InterruptedException If the calling thread is cancelled
	 */
	private void waitParticipantsTermination(Thread[] participants, long terminationWaitTime) throws InterruptedException
	{
		// Wait for participants termination
		for(int i=0; i<participants.length; ++i)
		{
			participants[i].join(terminationWaitTime);
			if(participants[i].isAlive())
				fail("One participant thread remains blocked");
		}
	}
	
	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}
	
	@Test
	public final void testGateNormalOperation()
	{
		try {
			final int THREAD_COUNT = 4;
			final Gate gate = new Gate();
			final long START_WAIT_TIME = 100;
			
			final Thread[] participants = startParticipants(gate, THREAD_COUNT, START_WAIT_TIME);
			
			gate.open();

			final long TERMINATION_WAIT_TIME = 1000;
			waitParticipantsTermination(participants, TERMINATION_WAIT_TIME);
		} 
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
	
	public final void testNormalOperation()
	{
		try {
			final int THREAD_COUNT = 4;
			final Gate gate = new Gate();
			
			// Start participant threads
			final Thread[] participants = new Thread[THREAD_COUNT];
			for(int i = 0; i<participants.length; ++i)
				(participants[i] = new Thread() {
					public void run() 
					{ 
						try { gate.await(); }
						catch(InterruptedException unexpected) 
						{
							threadFail("Participant thread was interrupted");
						}
					}
				}).start();
			
			// Wait for participants to reach the gate
			final long START_WAIT_TIME = 100;
			Thread.sleep(START_WAIT_TIME);
		
			// Open the gate
			gate.open();
			
			// Wait for participants termination
			final long TERMINATION_WAIT_TIME = 1000;
			for(int i=0; i<participants.length; ++i)
			{
				participants[i].join(TERMINATION_WAIT_TIME);
				if(participants[i].isAlive())
					fail("One participant thread remains blocked");
			}
		}
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
	
	@Test
	public final void testOpenFollowedByClose()
	{
		try {
			final int THREAD_COUNT = 4;
			final Gate gate = new Gate();
			final long START_WAIT_TIME = 100;
			
			final Thread[] participants = startParticipants(gate, THREAD_COUNT, START_WAIT_TIME);
			
			// Open the gate and immediately close it. At this point all 
			// participants are expected to be blocked at the gate, and
			// therefore all of them must be signaled
			gate.open();
			gate.close();
			
			final long TERMINATION_WAIT_TIME = 1000;
			waitParticipantsTermination(participants, TERMINATION_WAIT_TIME);
		}
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
	
	@Test
	public final void testParticipantsCancellation()
	{
		try {
			final int THREAD_COUNT = 4;
			final Gate gate = new Gate();
			final long START_WAIT_TIME = 100;
			
			final Thread[] participants = startParticipants(gate, THREAD_COUNT, START_WAIT_TIME);
			
			// Cancel threads with even ids
			for(int i=0; i<participants.length; ++i)
			{
				if(i%2==0) 
					participants[i].interrupt();
			}
			
			// Open the gate
			gate.open();
						
			final long TERMINATION_WAIT_TIME = 1000;
			waitParticipantsTermination(participants, TERMINATION_WAIT_TIME);
		}
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
	
}
