package coriant.ks.cp.tests.demos.synchronizers;

import static org.junit.Assert.*;
import coriant.ks.cp.demos.synchronizers.Exchanger;
import coriant.ks.cp.tests.BaseTestCase;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.*;

import org.junit.*;

public class ExchangerTestsJ8 extends BaseTestCase {
	
	/**
	 * Utility method that fills the given array with the elements produced by
	 * the received function.
	 * 
	 * @param array The array to be filled
	 * @param elementSupplier The function that supplies the elements to be stored 
	 * in the array. The function receives the index where the returned element
	 * will be stored. 
	 * @return The array that was filled
	 */
	private static <T> T[] fillArray(T[] array, IntFunction<T> elementSupplier)
	{
		for(int i = 0; i < array.length; ++i)
			array[i] = elementSupplier.apply(i);
		
		return array;
	}
	
	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}

	@Test
	public final void testNormalOperation()
	{
		try {
			// In Java 8, just for fun =)
			final int PARTICIPANTS_COUNT = 2;
			final Exchanger<Integer> tradeBox = new Exchanger<>();
			final CountDownLatch running = new CountDownLatch(PARTICIPANTS_COUNT);
			
			// Prepare data set to be used in the unit test
			final int ELEMENT_COUNT = (1 << 10);
			final Integer[] thread1Input = fillArray(new Integer[ELEMENT_COUNT], index -> index);
			final Integer[] thread2Input = fillArray(new Integer[ELEMENT_COUNT], index -> ELEMENT_COUNT - index);
			
			final Integer[] thread1Output = new Integer[ELEMENT_COUNT];
			final Integer[] thread2Output = new Integer[ELEMENT_COUNT];

			// The following arrays are used merely to enable generalization
			Integer[][] inputs = { thread1Input, thread2Input };
			Integer[][] outputs = { thread1Output, thread2Output };
			
			// Start participants
			for(int i = 0; i < PARTICIPANTS_COUNT; ++i)
			{
				final int dataSetIndex = i; 
				new Thread(() -> {
					try {
						for(int idx = 0; idx < inputs[dataSetIndex].length; ++idx)
							outputs[dataSetIndex][idx] = tradeBox.exchange(inputs[dataSetIndex][idx]);
					} 
					catch (InterruptedException unexpected) 
					{
						threadFail("Participant thread was interrupted");
					}
					finally 
					{
						running.countDown();
					}
				}).start();
			}
			
			// Wait for participants to complete and assert that they terminated within the
			// estimated timeout. The timeout value depends on the input data set size.
			final long COMPLETION_TIMEOUT = 2000;
			assertTrue(running.await(COMPLETION_TIMEOUT, TimeUnit.MILLISECONDS));
			
			// Check data that resulted from the exchange
			assertArrayEquals(thread2Input, thread1Output);
			assertArrayEquals(thread1Input, thread2Output);
		}
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
}
