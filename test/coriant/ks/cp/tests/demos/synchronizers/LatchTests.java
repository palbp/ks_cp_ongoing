package coriant.ks.cp.tests.demos.synchronizers;

import static org.junit.Assert.*;
import coriant.ks.cp.demos.synchronizers.Latch;
import coriant.ks.cp.tests.BaseTestCase;

import org.junit.*;

public class LatchTests extends BaseTestCase {
	
	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}

	@Test
	public final void testCancellation()
	{
		try {
			final Latch latch = new Latch();
			
			final Thread awaiter = new Thread() {
				public void run()
				{
					try { 
						latch.await();
						threadFail("Unexpected await return");
					}
					catch(InterruptedException success) { }
				}
			};
			awaiter.start();
		
			// Wait for waiter thread to reach the latch
			// Implementation note: We should use explicit synchronization (e.g. CountDownLatch
			// or CyclicBarrier) to ensure that unfortunate timings do not result in test failure. 
			final long START_WAIT_TIME = 100;
			Thread.sleep(START_WAIT_TIME);
			
			// Cancel waiter thread
			awaiter.interrupt();
			
			// Wait for termination
			final long TERMINATION_WAIT_TIME = 1000;
			awaiter.join(TERMINATION_WAIT_TIME);
			assertFalse(awaiter.isAlive());
		}
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
	
	@Test
	public final void testAwaitTimeout()
	{
		try {
			final Latch latch = new Latch();
			
			final int TIMEOUT = 200; 
			final Thread awaiter = new Thread() {
				public void run()
				{
					try {
						threadAssertFalse(latch.await(TIMEOUT));
					}
					catch(InterruptedException unexpected) {
						threadFail("Participant thread was interrupted");
					}
				}
			};
			awaiter.start();

			// Wait for waiter to give up
			final int LOCKUP_DETECTION_TIMEOUT = TIMEOUT * 2;
			awaiter.join(LOCKUP_DETECTION_TIMEOUT);
			assertFalse(awaiter.isAlive());
		}
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
	
	@Test
	public final void testNormalOperation()
	{
		class WaiterThread extends Thread
		{
			private final Latch latch;
			public WaiterThread(Latch latch) { this.latch = latch; }
			public void run() 
			{ 
				try { latch.await(); }
				catch(InterruptedException unexpected) 
				{
					threadFail("Participant thread was interrupted");
				}
			}
		}
		
		try {
			final int THREAD_COUNT = 4;
			final Latch latch = new Latch();
			
			// Start participant threads
			final Thread[] participants = new Thread[THREAD_COUNT];
			// Start some threads that will arrive at the latch while it 
			// is still closed
			for(int i = 0; i < participants.length/2; ++i)
				(participants[i] = new WaiterThread(latch)).start();
		
			// Wait for threads to reach the latch
			final long START_WAIT_TIME = 100;
			Thread.sleep(START_WAIT_TIME);
			
			latch.open();
			
			// Start the remaining threads that will arrive at the latch
			// after it has been opened
			for(int i = participants.length/2; i < participants.length; ++i)
				(participants[i] = new WaiterThread(latch)).start();

			// Wait for participants termination
			final long TERMINATION_WAIT_TIME = 1000;
			for(int i=0; i<participants.length; ++i)
			{
				participants[i].join(TERMINATION_WAIT_TIME);
				if(participants[i].isAlive())
					fail("One participant thread remains blocked");
			}
		}
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
	
	
}
