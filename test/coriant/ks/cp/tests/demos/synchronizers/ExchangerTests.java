package coriant.ks.cp.tests.demos.synchronizers;

import static org.junit.Assert.*;
import coriant.ks.cp.demos.synchronizers.Exchanger;
import coriant.ks.cp.tests.BaseTestCase;
import coriant.ks.cp.utils.Functions.FuncNoError;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.*;

public class ExchangerTests extends BaseTestCase {
	
	/**
	 * Utility method that fills the given array with the elements produced by
	 * the received function.
	 * 
	 * @param array The array to be filled
	 * @param elementSupplier The function that supplies the elements to be stored 
	 * in the array. The function receives the index where the returned element
	 * will be stored. 
	 * @return The array that was filled
	 */
	private static <T> T[] fillArray(T[] array, FuncNoError<T, Integer> elementSupplier)
	{
		for(int i = 0; i < array.length; ++i)
			array[i] = elementSupplier.call(i);
		
		return array;
	}
	
	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
		threadFailureCollect();
	}

	@Test
	public final void testNormalOperation()
	{
		final int PARTICIPANTS_COUNT = 2;
		final Exchanger<Integer> tradeBox = new Exchanger<>();
		final CountDownLatch running = new CountDownLatch(PARTICIPANTS_COUNT);
		
		class ExchangerThread extends Thread
		{
			private final Integer[] input;
			private final Integer[] output;
			
			public ExchangerThread(Integer[] input, Integer[] output)
			{
				assert input.length == output.length;
				this.input = input;
				this.output = output;
			}
			
			public void run()
			{
				try {
					for(int i = 0; i < input.length; ++i)
						output[i] = tradeBox.exchange(input[i]);
				} 
				catch (InterruptedException unexpected) 
				{
					threadFail("Participant thread was interrupted");
				}
				finally 
				{
					running.countDown();
				}
			}
		}
		
		try {
			// Prepare data set to be used in the unit test
			final int ELEMENT_COUNT = (1 << 10);
			final Integer[] thread1Input = fillArray(new Integer[ELEMENT_COUNT], new FuncNoError<Integer, Integer>() {
				@Override public Integer call(Integer index) { return index; }
			});
			final Integer[] thread2Input = fillArray(new Integer[ELEMENT_COUNT], new FuncNoError<Integer, Integer>() {
				@Override public Integer call(Integer index) { return ELEMENT_COUNT - index; }
			});
			
			final Integer[] thread1Output = new Integer[ELEMENT_COUNT];
			final Integer[] thread2Output = new Integer[ELEMENT_COUNT];
			
			// Start participants
			new ExchangerThread(thread1Input, thread1Output).start();
			new ExchangerThread(thread2Input, thread2Output).start();
			
			// Wait for participants to complete and assert that they terminated within the
			// estimated timeout
			final long COMPLETION_TIMEOUT = 2000;
			assertTrue(running.await(COMPLETION_TIMEOUT, TimeUnit.MILLISECONDS));
			
			// Check data that resulted from the exchange
			assertArrayEquals(thread2Input, thread1Output);
			assertArrayEquals(thread1Input, thread2Output);
		}
		catch(Exception unexpected)
		{
			throw new UnknownError("Unexpected exception in test harness thread");
		}
	}
}
