/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session3;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A wait-free reentrant lock implementation.   
 * 
 * TODO: Fix existing code and produce a complete implementation 
 */
@SuppressWarnings("unused")
public final class SpinLock {

	/**
	 * The number of processors made available to the JVM at the target machine.
	 */
	private static final int PROCESSOR_COUNT = Runtime.getRuntime().availableProcessors(); 

	/**
	 * Constant value used to indicate that the lock is free.
	 */
	private static final int FREE = 0;

	/**
	 * Holds the thread-id of the current owner, or {@code 0} if the lock is free.
	 */
	private long _owner = FREE;
	
	/**
	 * The recursion counter.
	 */
	private int _counter = 0;

	/**
	 * Helper method that defines what happens in each spin iteration
	 */
	private static void SpinStep()
	{
		// This should be more elaborate, but it is not our focus  
		if(PROCESSOR_COUNT == 1) { Thread.yield(); }
	}
	
	/**
	 * Acquires the lock, incrementing the recursion counter if appropriate.  
	 */
	public void lock()
	{
		// TODO
	}
	
	/**
	 * Frees the lock, if the recursion counter as reached {@code 0}.
	 */
	public void unlock()
	{
		// TODO
	}
}
