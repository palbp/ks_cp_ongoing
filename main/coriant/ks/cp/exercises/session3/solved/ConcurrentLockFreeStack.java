/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session3.solved;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Lock-free stack implementation supported on a linked list to store the values.   
 * 
 * @param <T> The type of the contained elements.
 */
public final class ConcurrentLockFreeStack<T> {

	/**
	 * The number of processors made available to the JVM at the target machine.
	 */
	private static final int PROCESSOR_COUNT = Runtime.getRuntime().availableProcessors(); 

	/**
	 * Helper method that defines what happens while spinning
	 */
	private static void SpinStep()
	{
		// This should be more elaborate, but it is not our focus  
		if(PROCESSOR_COUNT == 1) { Thread.yield(); }
	}
	
	/**
	 * The backing linked-list node's type.
	 */
	private static class Node<E>
	{
		/**
		 * The element stored in the node.
		 */
		public final E _elem;
		
		/**
		 * The link to the next node.
		 */
		public Node<E> _next;
		
		/**
		 * Initiates an instance with the given element.
		 * @param elem the element to be stored
		 */
		public Node(E elem) { _elem = elem; }
	}
	
	/**
	 * Holds the list's head. 
	 */
	private final AtomicReference<Node<T>> _head = new AtomicReference<>(null);
	
	/**
	 * Adds the given element to the top of the stack.
	 * @param elem the element to be pushed
	 */
	public void push(T elem)
	{
		// Create node and store element
		Node<T> newNode = new Node<>(elem);
		
		while(true)
		{
			// Get current head and optimistically set the new node's next link to its value
			Node<T> observedHead = _head.get();  
			newNode._next = observedHead;

			// Atomically check if the head is unchanged and set it to the new node
			// If this fails, we start over
			if(_head.compareAndSet(observedHead, newNode))
				return;
			
			// Failed. Start over while spinning.
			SpinStep();
		} 
	}
	
	/**
	 * Removes an element from the top of the stack, returning it.
	 * @return the popped element, or {@code null} if none exists
	 */
	public T pop()
	{
		while(true)
		{
			// Get current head
			Node<T> observedHead = _head.get();
			
			if(observedHead == null)
				return null;
				
			// Atomically check if the head is unchanged and set it to the observed's head next node
			// If this fails, we start over
			if(_head.compareAndSet(observedHead, observedHead._next))
				return observedHead._elem;

			// Failed. Start over while spinning.
			SpinStep();
		} 
	}
	
	/**
	 * Returns a boolean value indicating if the stack is empty.
	 * @return {@code true}, if the stack is empty, {@code false} otherwise.
	 */
	public boolean isEmpty()
	{
		return _head.get() == null;
	}
}
