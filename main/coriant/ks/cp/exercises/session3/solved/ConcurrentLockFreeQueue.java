/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session3.solved;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Lock-free queue implementation supported on a linked list to store the values.   
 * 
 * In this implementation, each node uses an AtomicReference, therefore increasing 
 * space overhead and GC pressure.
 * 
 * @param <T> The type of the contained elements.
 */
public final class ConcurrentLockFreeQueue<T> {

	/**
	 * The number of processors made available to the JVM at the target machine.
	 */
	private static final int PROCESSOR_COUNT = Runtime.getRuntime().availableProcessors(); 

	/**
	 * Helper method that defines what happens while spinning
	 */
	private static void SpinStep()
	{
		// This should be more elaborate, but it is not our focus  
		if(PROCESSOR_COUNT == 1) { Thread.yield(); }
	}

	/**
	 * The backing linked-list node's type.
	 */
	private static class Node<E>
	{
		/**
		 * The element stored in the node.
		 */
		public final E _elem;
		
		/**
		 * The link to the next node.
		 */
		public final AtomicReference<Node<E>> _next;
		
		/**
		 * Initiates an instance with the given element.
		 * @param elem the element to be stored
		 */
		public Node(E elem) { _elem = elem; _next = new AtomicReference<>(null); }
	}
	
	/**
	 * Holds the list's head. 
	 */
	private final AtomicReference<Node<T>> _head;
	
	/**
	 * Holds the list's tail. 
	 */
	private final AtomicReference<Node<T>> _tail;
	
	/**
	 * Initiates an instance
	 */
	public ConcurrentLockFreeQueue()
	{
		Node<T> dummy = new Node<>(null);
		_head = new AtomicReference<>(dummy);
		_tail = new AtomicReference<>(dummy);
	}
	
	/**
	 * Adds the given element to the end of the queue.
	 * @param elem the element to be added
	 */
	public void put(T elem)
	{
		Node<T> newNode = new Node<>(elem);

		// Insert node by appending it to the tail
		Node<T> observedTail;
		do {
			// Check if list's tail is out-of-date and update it
			while(true)
			{
				observedTail = _tail.get();
				// Is tail updated?
				if(observedTail._next.get() == null)
					break;
				_tail.compareAndSet(observedTail, observedTail._next.get());
			}
			
			// Tail is updated. Lets try to win the race to start the insertion. 
			// If we fail, we try again while updating the tail if it is out-of-date.
		} while (!observedTail._next.compareAndSet(null, newNode));
		
		// Finish the insertion by trying to update the tail. If we fail, the next put will update it.
		_tail.compareAndSet(observedTail, newNode);
	}
	
	/**
	 * Removes an element from the start of the queue, returning it.
	 * @return the removed element, or {@code null} if none exists
	 */
	public T take()
	{
		while(true)
		{
			Node<T> observedHead = _head.get();

			// Check if the queue is already empty
			if(observedHead._next.get() == null)
				return null;
			
			Node<T> elementToRemove = observedHead._next.get();
			
			// Try to update the list's head. If we fail, a concurrent take interfered. Try again.
			if(_head.compareAndSet(observedHead, elementToRemove))
				return elementToRemove._elem;
			
			SpinStep();
		}
	}
	
	/**
	 * Returns a boolean value indicating if the queue is empty.
	 * @return {@code true}, if the queue is empty, {@code false} otherwise.
	 */
	public boolean isEmpty()
	{
		return _head.get()._next.get() == null;
	}
}
