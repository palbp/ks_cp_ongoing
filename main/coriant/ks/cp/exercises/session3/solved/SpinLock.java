/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session3.solved;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A wait-free reentrant lock implementation.   
 */
public final class SpinLock {

	/**
	 * The number of processors made available to the JVM at the target machine.
	 */
	private static final int PROCESSOR_COUNT = Runtime.getRuntime().availableProcessors(); 

	/**
	 * Constant value used to indicate that the lock is free.
	 */
	private static final int FREE = 0;

	/**
	 * Holds the thread-id of the current owner, or {@code 0} if the lock is free.
	 */
	private final AtomicLong _owner = new AtomicLong(FREE);
	
	/**
	 * The recursion counter.
	 */
	private int _counter = 0;

	/**
	 * Helper method that defines what happens while spinning
	 */
	private static void SpinStep()
	{
		// This should be more elaborate, but it is not our focus  
		if(PROCESSOR_COUNT == 1) { Thread.yield(); }
	}
	
	/**
	 * Acquires the lock, incrementing the recursion counter if appropriate.  
	 */
	public void lock()
	{
		long currThreadId = Thread.currentThread().getId();

		// If the current thread does not hold the lock, try to acquire it
		long currOwner = _owner.get();
		if(currOwner != currThreadId)
		{
			do {
				// Implementation note:  
				// get is cheaper than compareAndSet, hence the inner while
				while(currOwner != FREE)
				{
					SpinStep();
					currOwner = _owner.get();
				}
				// Lock is free. Try to acquire it.
			}
			while(!_owner.compareAndSet(FREE, currThreadId));
		}

		_counter += 1; 
	}
	
	/**
	 * Frees the lock, if the recursion counter as reached {@code 0}.
	 */
	public void unlock()
	{
		if(_owner.get() != Thread.currentThread().getId())
			throw new IllegalStateException();
		
		if(--_counter == 0)
			_owner.set(FREE);
	}
}
