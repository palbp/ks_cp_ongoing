/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session3;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Lock-free queue implementation supported on a linked list to store the values.   
 * 
 * @param <T> The type of the contained elements.
 * 
 * TODO: Fix existing code and produce a complete implementation 
 */
@SuppressWarnings("unused")
public final class ConcurrentLockFreeQueue<T> {

	/**
	 * The backing linked-list node's type.
	 */
	private static class Node<E>
	{
		/**
		 * The element stored in the node.
		 */
		public E _elem;
		
		/**
		 * The link to the next node.
		 */
		public Node<E> _next;
		
		/**
		 * Initiates an instance with the given element.
		 * @param elem the element to be stored
		 */
		public Node(E elem) { _elem = elem; _next = null; }
	}
	
	/**
	 * Holds the list's head. 
	 */
	private Node<T> _head;
	
	/**
	 * Holds the list's tail. 
	 */
	private Node<T> _tail;
	
	/**
	 * Initiates an instance
	 */
	public ConcurrentLockFreeQueue()
	{
		Node<T> _dummy = new Node<>(null);
		_head = _tail = _dummy;
	}
	
	/**
	 * Adds the given element to the end of the queue.
	 * @param elem the element to be added
	 */
	public void put(T elem)
	{
		// TODO
	}
	
	/**
	 * Removes an element from the start of the queue, returning it.
	 * @return the removed element, or {@code null} if none exists
	 */
	public T take()
	{
		// TODO
		return null;
	}
	
	/**
	 * Returns a boolean value indicating if the queue is empty.
	 * @return {@code true}, if the queue is empty, {@code false} otherwise.
	 */
	public boolean isEmpty()
	{
		// TODO
		return true;
	}
}
