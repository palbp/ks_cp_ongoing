/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session3;

/**
 * Lock-free stack implementation supported on a linked list to store the values.   
 * 
 * @param <T> The type of the contained elements.
 * 
 * TODO: Fix existing code and produce a complete implementation 
 */
@SuppressWarnings("unused")
public final class ConcurrentLockFreeStack<T> {

	/**
	 * The backing linked-list node's type.
	 */
	private class Node
	{
		/**
		 * The element stored in the node.
		 */
		public T _elem;
		
		/**
		 * The link to the next node.
		 */
		public Node _next;
		
		/**
		 * Initiates an instance with the given element.
		 * @param elem the element to be stored
		 */
		public Node(T elem) { _elem = elem; }
	}
	
	/**
	 * Holds the list's head. 
	 */
	private Node _head;

	/**
	 * Adds the given element to the top of the stack.
	 * @param elem the element to be pushed
	 */
	public void push(T elem)
	{
		// TODO
	}
	
	/**
	 * Removes an element from the top of the stack, returning it.
	 * @return the popped element, or {@code null} if none exists
	 */
	public T pop()
	{
		// TODO
		return null;
	}
	
	/**
	 * Returns a boolean value indicating if the stack is empty.
	 * @return {@code true}, if the stack is empty, {@code false} otherwise.
	 */
	public boolean isEmpty()
	{
		// TODO
		return true;
	}
}
