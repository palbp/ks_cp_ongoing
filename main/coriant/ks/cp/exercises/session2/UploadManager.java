package coriant.ks.cp.exercises.session2;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

/**
 * Another implementation of a simplified version of PM's UploadManager.
 * This implementation provides an asynchronous interface based on
 * the future design pattern.
 * 
 * @param <TResult> the upload operation's result type
 * 
 * The class instances are thread-safe.
 */
public final class UploadManager<TResult>
{
	/**
	 * The interface which specifies the contract for objects that perform actual
	 * upload work.
	 *  
	 * @param <TResult> the upload operation's result type
	 */
	public static interface UploadWorker<TResult>
	{
		/**
		 * Method that contains the upload operations.
		 * 
		 * @param neId the NE identifier
		 * @param type the upload type
		 * @return the upload result
		 * @throws Exception the upload execution error, if one occurred  
		 */
		public TResult doUpload(int neId, UploadType type) throws Exception;
	}
	
	/**
	 * Initiates an instance that supports at most {@link maxSimultaneousUploads} simultaneous
	 * throttled uploads.
	 *   
	 * @param maxSimultaneousUploads the number of throttled simultaneous uploads to be supported
	 */
	public UploadManager(int maxSimultaneousUploads, UploadExecutionService<TResult> executor, UploadWorker<TResult> worker)
	{
		// TODO
	}
	
	/**
	 * Creates a throttled upload request for the given NE. The request is discarded if an upload 
	 * request of the same type and for for the same NE has already been accepted and not yet executed.
	 * 
	 * There are, at most, {@code maxSimultaneousUploads} throttled uploads being simultaneously executed 
	 * 
	 * @param neId The NE identifier
	 * @param type the upload type
	 * @return {@link UploadTask} a future representing the upload request
	 * @throws {@link IllegalStateException} if the upload request is rejected
	 */
	public UploadTask<TResult> performThrottledUpload(int neId, UploadType type) throws IllegalStateException
	{
		// TODO
		return null;
	}
	
	/**
	 * Creates an upload request for the given NE. The request is discarded if an upload 
	 * request of the same type and for for the same NE has already been accepted and not yet executed. 
	 * 
	 * @param neId The NE identifier
	 * @param type the upload type
	 * @return {@link UploadTask} a future representing the upload request
	 * @throws {@link IllegalStateException} if the upload request is rejected
	 */
	public UploadTask<TResult> performUpload(int neId, UploadType type) throws IllegalStateException
	{
		// TODO
		return null;
	}
}
