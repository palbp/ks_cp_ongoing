package coriant.ks.cp.exercises.session2;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

/**
 * Class that implements a synchronizer responsible for providing a synchronous 
 * interface for service provisioning operations, which are performed asynchronously.
 * 
 * This implementation makes use of the Specific Notification pattern, thus optimizing
 * the number of context-switches.
 * 
 * Class instances are thread-safe.
 * 
 * @author Paulo Pereira
 */
public final class ServiceProvisioningManager 
{
	/**
	 * Helper method used to simulate service provisioning actions.
	 *  
	 * @param serviceId The service identifier
	 */
	@SuppressWarnings("unused")
	private void submitProvisioningActions(final int serviceId)
	{
		// Simulate GM calls
		try { Thread.sleep(100); }
		catch(InterruptedException irrelevant) {}
	}
	
	/**
	 * Initiates a {@link ServiceProvisioningManager} instance.
	 */
	public ServiceProvisioningManager()
	{
		// TODO
	}
	
	/**
	 * Requests the provisioning of the service with the given identifier.
	 * If there is no pending request for creation of a service with the given 
	 * identifier, the calling thread is used to trigger service creation.
	 *
	 * The caller is blocked until service provisioning completion is signaled, 
	 * regardless of the existence of a previous pending request for the received
	 * identifier. 
	 * 
	 * @param serviceId The service identifier
	 * @throws InterruptedException
	 */
	public void provisionService(int serviceId) throws InterruptedException
	{
		// TODO
	}
	
	/**
	 * Signals completion of service provisioning for the service with the given 
	 * identifier, unblocking all threads waiting for its completion.
	 * 
	 * @param serviceId the service identifier
	 * @throws IllegalStateException if the received serviced identifier has no
	 * corresponding request
	 */
	public void provisionServiceComplete(int serviceId) throws IllegalStateException
	{
		// TODO
	}
}
