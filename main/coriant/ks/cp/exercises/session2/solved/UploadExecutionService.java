package coriant.ks.cp.exercises.session2.solved;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import java.util.*;

import coriant.ks.cp.utils.Functions;

/**
 * Class that implements the upload execution scheduling policy, that is,
 * it insures that, for any given NE, there is at most one upload in progress.
 * 
 * @param <TResult> the upload operation's result type
 * 
 * Class instances are thread-safe.
 * 
 * @author Paulo Pereira
 */
public final class UploadExecutionService<TResult> {

	/**
	 * Shared state that holds delayed upload tasks
	 * Guarded by {@code _delayed} intrinsic monitor.
	 */
	private final Queue<UploadTask<TResult>> _delayed;
	
	/**
	 * Shared state that holds ongoing upload tasks
	 * Guarded by {@code _delayed} intrinsic monitor.
	 */
	private final List<UploadTask<TResult>> _ongoing;

	/**
	 * Helper method that schedules for execution a delayed upload task
	 * for the NE with the given identifier.   
	 * 
	 * @param neId the NE identifier
	 * @return the next upload task for the NE with the received identifier,
	 * or {@code null} if none exists
	 */
	private UploadTask<TResult> scheduleNextDelayedForNE(int neId) 
	{
		synchronized(_delayed)
		{
			Iterator<UploadTask<TResult>> iterator = _delayed.iterator();
			while(iterator.hasNext())
			{
				UploadTask<TResult> current = iterator.next();
				if(current.getNeId() == neId)
				{
					iterator.remove();
					_ongoing.add(current);
					return current;
				}
			}
			return null;
		}
	}
	
	/**
	 * Helper method for removing the received upload task from the 
	 * ongoing upload list. This method is called after the execution
	 * of an upload task is completed.
	 * 
	 * @param futureRequest the upload task to remove from the ongoing list
	 */
	private void removeFromOngoing(UploadTask<TResult> futureRequest) 
	{
		synchronized (_delayed) 
		{
			_ongoing.remove(futureRequest);
		}
	}

	/**
	 * Helper method for starting a worker thread to execute the 
	 * received task. Once this task is executed, the thread gets
	 * more work from the {@code _delayed} queue until the queue
	 * has no more pending uploads for the same NE. Only then does 
	 * the thread terminate.
	 * 
	 * @param futureRequest the task to be executed
	 */
	private void startWork(final UploadTask<TResult> futureRequest) 
	{
		// Unbounded thread creation: not a good idea!
		(new Thread() {
			@Override
			public void run()
			{
				UploadTask<TResult> current = futureRequest;
				while(current != null)
				{
					current.run();
					current = scheduleNextDelayedForNE(current.getNeId());
				}
			}
		}).start();
	}
	
	/**
	 * Helper method for scheduling the received upload task.
	 * The task is rejected if an upload for the same NE is already in 
	 * progress. 
	 * 
	 * Must be called while owning {@code _delayed}'s intrinsic lock
	 *  
	 * @param futureRequest
	 * @return
	 */
	private boolean tryPutOnGoing(UploadTask<TResult> futureRequest) 
	{
		// Reject if a conflicting upload is ongoing
		for(UploadTask<TResult> current : _ongoing)
			if(current.getNeId() == futureRequest.getNeId())
				return false;
		
		// Not rejected! Start work.
		_ongoing.add(futureRequest);
		futureRequest.whenDone(new Functions.Action<UploadTask<TResult>>() {
			@Override
			public void call(UploadTask<TResult> completedTask) { removeFromOngoing(completedTask); }
		});
		startWork(futureRequest);
		return true;
	}

	/**
	 * Initiates an instance
	 */
	public UploadExecutionService()
	{
		_delayed = new LinkedList<>();
		_ongoing = new LinkedList<>();
	}
	
	/**
	 * Method that adds the received upload task to the execution queue 
	 * if an upload for the same NE is not already being made.
	 * 
	 * @param futureRequest the upload task to be executed
	 * @return {@code true} if the upload request has been accepted, 
	 * {@code false} otherwise 
	 */
	public boolean offer(UploadTask<TResult> futureRequest)
	{
		synchronized (_delayed) 
		{
			return tryPutOnGoing(futureRequest);
		}
	}
	
	/**
	 * Method that adds the received upload task to the execution queue. 
	 * 
	 * @param futureRequest the upload task to be executed
	 */
	public void put(UploadTask<TResult> futureRequest)
	{
		synchronized (_delayed) 
		{
			if(!tryPutOnGoing(futureRequest))
				_delayed.add(futureRequest);
		}
	}
}
