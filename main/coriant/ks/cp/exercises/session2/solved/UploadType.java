package coriant.ks.cp.exercises.session2.solved;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

/**
 * Enumeration used to specify the type of upload. 
 */
public enum UploadType {

	INTERVAL_15_MIN, INTERVAL_24_HRS 
}