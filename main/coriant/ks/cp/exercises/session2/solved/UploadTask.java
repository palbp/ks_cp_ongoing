package coriant.ks.cp.exercises.session2.solved;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import java.util.*;
import java.util.concurrent.*;

import coriant.ks.cp.utils.Functions;

/**
 * Class whose instances represent upload tasks. 
 * 
 * Although the implementation is based on the future task pattern, 
 * it has several limitations; it does not intend to be a full 
 * optimized implementation of a {@link java.util.concurrent.FutureTask}
 * 
 * <ul>This version's properties:
 * 		<li> task cancellation is not supported </li>
 * 		<li> task chaining is not supported (in continuations) </li> 
 * </ul>
 * 
 * @param <T> the upload operation's result type
 * 
 * Class instances are thread-safe.
 */
public final class UploadTask<T> implements Runnable
{
	/**
	 * Enumeration with the possible values for upload task status. 
	 */
	public static enum Status { PENDING, INPROGRESS, COMPLETED }
	
	/**
	 * Immutable state holding the NE identifier.
	 */
	private final int _neId;
	
	/**
	 * Immutable state holding the upload type.
	 */
	private final UploadType _type;
	
	/**
	 * Immutable state holding the task's associated action.
	 */
	private final Callable<T> _taskAction;
	
	/**
	 * Mutable shared state that holds the upload status.
	 * Guarded by {@code _whenDoneActions}'s intrinsic monitor.
	 */
	private Status _status;
	
	/**
	 * Mutable shared state that holds the upload result, if already available. 
	 * Guarded by {@code _whenDoneActions}'s intrinsic monitor.
	 */
	private T _result;

	/**
	 * Mutable shared state that holds the upload error, if one occurred.
	 * Guarded by {@code _whenDoneActions}'s intrinsic monitor.
	 */
	private Throwable _error;
	
	/**
	 * Mutable shared state that holds the instance's completion actions.
	 * Guarded by its own intrinsic monitor.
	 */
	private final List<Functions.Action<UploadTask<T>>> _whenDoneActions;
	
	/**
	 * Mutable shared state that holds the instance's continuations.
	 * Guarded by {@code _whenDoneActions}'s intrinsic monitor.
	 */
	private final List<Functions.Action<UploadTask<T>>> _continuationActions;

	/**
	 * Helper method that registers the given action to the received list.
	 * 
	 * @param action The action to be registered
	 * @param list The action list to which {@code action} will be added.
	 * @return The current task instance, to enable fluent usage
	 */
	private UploadTask<T> addRegistration(Functions.Action<UploadTask<T>> action, 
				List<Functions.Action<UploadTask<T>>> list) 
	{
		synchronized (_whenDoneActions)
		{
			// Task already in progress or completed. Registrations are no longer allowed.
			if(_status != Status.PENDING)
				throw new IllegalStateException();
			
			list.add(action);
			return this;
		}
	}
	
	/**
	 * Publishes the given upload task status.
	 *  
	 * @param status the new upload task status
	 */
	private void setStatus(Status status)
	{
		synchronized (_whenDoneActions)
		{
			_status = status;
		}
	}
	
	/**
	 * Publishes the given result and the given error (if not {@code null}).
	 * 
	 * @param result the upload result
	 * @param error the upload error or {@code null} if none occurred
	 */
	private void setResult(T result, Throwable error)
	{
		synchronized (_whenDoneActions)
		{
			_error = error;
			_result = result;
			_status = Status.COMPLETED;
			_whenDoneActions.notifyAll();
			
			for(Functions.Action<UploadTask<T>> action : _whenDoneActions)
				action.call(this);
		}
	}
	
	/**
	 * Initiates an instance with the given arguments.
	 * 
	 * @param neId the request's NE identifier 
	 * @param type request's upload type
	 * @param action the task's action 
	 */
	public UploadTask(int neId, UploadType type, Callable<T> action)
	{
		_neId = neId;
		_type = type;
		_status = Status.PENDING;
		_error = null;
		_result = null;
		_taskAction = action;
		_whenDoneActions = new LinkedList<>();
		_continuationActions = new LinkedList<>();
	}
	
	/**
	 * Gets the upload request's NE identifier.
	 * @return the request's NE identifier
	 */
	public int getNeId() { return _neId; }
	
	/**
	 * Gets the upload request's upload type.
	 * @return
	 */
	public UploadType getUploadType() { return _type; }
	
	/**
	 * Gets the request's upload type identifier.
	 * @return the request's upload type identifier
	 */
	public Status getStatus()
	{
		synchronized (_whenDoneActions)
		{
			return _status;
		}
	}
	
	/**
	 * Gets the upload request's result, blocking the calling thread until the result 
	 * is available or the specified time interval {@code timeout} has elapsed.
	 *  
	 * @param timeout the number of milliseconds the calling thread is willing to wait 
	 * @return The upload result
	 * @throws ExecutionException if an error while performing the upload
	 * @throws InterruptedException if the calling thread was interrupted
	 * @throws TimeoutException if the specified timeout has elapsed
	 */
	public T getResult(long timeout) throws ExecutionException,  
													TimeoutException, InterruptedException
	{
		synchronized (_whenDoneActions)
		{
			if(_status == Status.COMPLETED)
			{
				if(_error != null)
					throw new ExecutionException(_error);
				return _result;
			}
			
			// Get a temporal reference (INFINITY == 0)
			long waitStart = (timeout != 0) ? System.currentTimeMillis() : 0;
			
			while(true)
			{
				_whenDoneActions.wait(timeout);
				
				// Check if thread was signaled
				if(_status == Status.COMPLETED)
				{
					if(_error != null)
						throw new ExecutionException(_error);
					return _result;
				}
				
				// Check for timeout (if not INFINITY)
				if(timeout != 0)
				{
					long now = System.currentTimeMillis();
					long elapsed = now - waitStart;
					
					if(elapsed >= timeout) // Thread gave up. 
						throw new TimeoutException(); 
					
					timeout -= elapsed;
					waitStart = now;
				}
			}
		}
	}
	
	/**
	 * Gets the upload request's result, blocking the calling thread until the result 
	 * is available.
	 *  
	 * @return The upload result
	 * @throws ExecutionException if an error while performing the upload
	 * @throws InterruptedException if the calling thread was interrupted
	 */
	public T getResult() throws ExecutionException, InterruptedException
	{
		try {
			return getResult(0); // INFINITY = 0
		} catch (TimeoutException impossible) 
		{ 
			// It won't happen!
			UnknownError error = new UnknownError();
			error.initCause(impossible);
			throw error;
		}
	}
	
	/**
	 * Registers the given action to the list of actions that are executed 
	 * to after signaling completion.
	 * 
	 * @param action The action to be registered
	 * @return The current task instance, to enable fluent usage
	 */
	public UploadTask<T> whenDone(Functions.Action<UploadTask<T>> action) 
	{
		return addRegistration(action, _whenDoneActions);
	}
	
	/**
	 * Registers the given action to the list of continuation actions. These
	 * actions are executed after the execution of all actions registered 
	 * with {@link whenDone}. 
	 * 
	 * @param action The action to be registered
	 * @return The current task instance, to enable fluent usage
	 */
	public UploadTask<T> continueWith(Functions.Action<UploadTask<T>> action) 
	{
		return addRegistration(action, _continuationActions);
	}

	/**
	 * Method that contains the sequence of actions performed by task instances.
	 * This method is to be called by the thread that will execute the task's work. 
	 * 
	 * The sequence of actions is as follows:
	 * <ol>
	 * 	<li> Status is set to {@link Status.INPROGRESS} </li>
	 * 	<li> The task action is called and the result is stored </li>
	 * 	<li> Status is set to {@link Status.COMPLETED} </li>
	 *  <li> Registered when-done actions are executed </li>
	 * 	<li> Threads that are blocked waiting for the task's result are signaled </li>
	 *	<li> Continuation actions are executed </li>
	 * </ol>
	 */
	public void run()
	{
		setStatus(Status.INPROGRESS);
		
		// Call task action and store result 
		Exception error = null;
		T result = null;
		try { result = _taskAction.call(); }
		catch(Exception e) { error = e; }
		
		// Publish task result, signal blocked threads and execute completion actions (when-done)
		setResult(result, error);
		
		// Execute continuations
		// Implementation note: visibility is ensured through piggyback  
		for(Functions.Action<UploadTask<T>> action : _continuationActions)
			action.call(this);
	}
}