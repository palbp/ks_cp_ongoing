package coriant.ks.cp.exercises.session2.solved;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Callable;

import coriant.ks.cp.utils.Functions;

/**
 * Another implementation of a simplified version of PM's UploadManager.
 * This implementation provides an asynchronous interface based on
 * the future design pattern.
 * 
 * @param <TResult> the upload operation's result type
 * 
 * The class instances are thread-safe.
 * 
 * @author Paulo Pereira
 */
public final class UploadManager<TResult>
{
	/**
	 * The interface which specifies the contract for objects that perform actual
	 * upload work.
	 *  
	 * @param <TResult> the upload operation's result type
	 */
	public static interface UploadWorker<TResult>
	{
		/**
		 * Method that contains the upload operations.
		 * 
		 * @param neId the NE identifier
		 * @param type the upload type
		 * @return the upload result
		 * @throws Exception the upload execution error, if one occurred  
		 */
		public TResult doUpload(int neId, UploadType type) throws Exception;
	}
	
	/**
	 * Shared state that holds pending upload tasks. Tasks are added to 
	 * this queue if they are made by using a throttled request and the 
	 * maximum count of ongoing simultaneous throttled requests has been 
	 * reached.
	 * Guarded by {@code _throttledQueue} intrinsic monitor.
	 */
	private final Queue<UploadTask<TResult>> _throttledQueue;
	
	/**
	 * Shared state that holds ongoing upload tasks.
	 * This information is replicated in the {@link UploadManager} and 
	 * {@link UploadExecutionService} implementations. This could be avoided
	 * with the techniques presented in Session 3.  
	 * Guarded by {@code _throttledQueue} intrinsic monitor.
	 */
	private final List<UploadTask<TResult>> _ongoing;

	/**
	 * The maximum number of simultaneous throttled uploads allowed.
	 */
	private final int _maxSimultaneousUploads;

	/**
	 * The current number of simultaneous throttled uploads.
	 */
	private int _currentSimultaneousUploads;
	
	/**
	 * The upload executor service to be used.
	 */
	private final UploadExecutionService<TResult> _executor;
	
	/**
	 * The actual upload work. 
	 */
	private final UploadWorker<TResult> _worker;
	
	/**
	 * Helper method that creates the upload request if it is not redundant.
	 * Redundant requests are discarded.
	 *  
	 * @param neId the NE identifier
	 * @param type the upload type
	 * @param isThrottled boolean value indicating whether it is a throttled 
	 * request or not.  
	 * @return the upload request instance or {@code null} if it is redundant.
	 * otherwise. 
	 */
	private UploadTask<TResult> createRequest(final int neId, final UploadType type, final boolean isThrottled)
	{
		// Is an equivalent request in progress?
		for(UploadTask<TResult> current : _ongoing)
			if(current.getNeId() == neId && current.getUploadType().equals(type))
				return null;
		
		// Check if an equivalent request is in the throttled requests' queue
		Iterator<UploadTask<TResult>> itr = _throttledQueue.iterator();
		while(itr.hasNext())
		{
			UploadTask<TResult> current = itr.next();
			if(current.getNeId() == neId && current.getUploadType().equals(type))
			{
				// Found an equivalent request. Should we consider it redundant?
				if(isThrottled)
					return null; // The request is redundant
				
				// Promote the existing request to non-throttled
				itr.remove();
				return current;
			}
		}
		
		// Request is not redundant. Create the future.
		final UploadTask<TResult> future = new UploadTask<TResult>(neId, type, new Callable<TResult>() {
			@Override
			public TResult call() throws Exception
			{
				return _worker.doUpload(neId, type);
			}
		}).continueWith(new Functions.Action<UploadTask<TResult>>() {
			@Override
			public void call(UploadTask<TResult> continuedTask)
			{
				synchronized(_throttledQueue)
				{
					// Remove continued task from ongoing list
					_ongoing.remove(continuedTask);
					
					if(isThrottled)
						_currentSimultaneousUploads -= 1;
					
					// try to schedule a throttled request
					if((_currentSimultaneousUploads < _maxSimultaneousUploads) && !_throttledQueue.isEmpty())
					{
						if(_executor.offer(_throttledQueue.peek()))
						{
							// Request was accepted. 
							_ongoing.add(_throttledQueue.remove());
							_currentSimultaneousUploads += 1;
						}
					}
				}
			}
		});
		
		return future;
	}
	
	/**
	 * Initiates an instance that supports at most {@link maxSimultaneousUploads} simultaneous
	 * throttled uploads.
	 *   
	 * @param maxSimultaneousUploads the number of throttled simultaneous uploads to be supported
	 */
	public UploadManager(int maxSimultaneousUploads, UploadExecutionService<TResult> executor, UploadWorker<TResult> worker)
	{
		_maxSimultaneousUploads = maxSimultaneousUploads;
		_currentSimultaneousUploads = 0;
		_throttledQueue = new LinkedList<>();
		_ongoing = new LinkedList<>();
		_executor = executor;
		_worker = worker;
	}
	
	/**
	 * Creates a throttled upload request for the given NE. The request is discarded if an upload 
	 * request of the same type and for for the same NE has already been accepted and not yet executed.
	 * 
	 * There are, at most, {@code maxSimultaneousUploads} throttled uploads being simultaneously executed 
	 * 
	 * @param neId The NE identifier
	 * @param type the upload type
	 * @return {@link UploadTask} a future representing the upload request
	 * @throws {@link IllegalStateException} if the upload request is rejected
	 */
	public UploadTask<TResult> performThrottledUpload(int neId, UploadType type) throws IllegalStateException
	{
		synchronized(_throttledQueue)
		{
			UploadTask<TResult> request = createRequest(neId, type, true);

			// Check if request is redundant
			if(request == null)
				throw new IllegalStateException();

			// The request is throttled. May we submit it for execution?
			if(_currentSimultaneousUploads < _maxSimultaneousUploads)
			{
				if(_executor.offer(request))
				{
					// Request was accepted. 
					_ongoing.add(request);
					_currentSimultaneousUploads += 1;
					return request;
				}
			}
			
			// Request can not be submitted for execution. Either the maximum count of simultaneous  
			// throttled uploads as been reached, or the execution service detected a conflict.
			// In both cases, add the request to pending queue. 
			_throttledQueue.add(request);
			return request;
		}
	}
	
	/**
	 * Creates an upload request for the given NE. The request is discarded if an upload 
	 * request of the same type and for for the same NE has already been accepted and not yet executed. 
	 * 
	 * @param neId The NE identifier
	 * @param type the upload type
	 * @return {@link UploadTask} a future representing the upload request
	 * @throws {@link IllegalStateException} if the upload request is rejected
	 */
	public UploadTask<TResult> performUpload(int neId, UploadType type) throws IllegalStateException
	{
		synchronized(_throttledQueue)
		{
			UploadTask<TResult> request = createRequest(neId, type, false);

			// Check if request is redundant
			if(request == null)
				throw new IllegalStateException();
			
			// Submit request for execution
			_executor.put(request);
			_ongoing.add(request);
			return request;
		}
	}
}
