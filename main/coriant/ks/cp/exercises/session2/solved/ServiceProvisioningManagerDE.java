package coriant.ks.cp.exercises.session2.solved;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import java.util.*;

/**
 * Class that implements a synchronizer responsible for providing a synchronous 
 * interface for service provisioning operations, which are performed asynchronously.
 * 
 * This implementation makes use of the Delegation of Execution pattern.
 * 
 * Class instances are thread-safe.
 * 
 * @author Paulo Pereira
 */
public final class ServiceProvisioningManagerDE 
{
	/**
	 * Helper method used to simulate service provisioning actions.
	 *  
	 * @param serviceId The service identifier
	 */
	private void submitProvisioningActions(final int serviceId)
	{
		// Simulate GM calls
		try { Thread.sleep(100); }
		catch(InterruptedException irrelevant) {}
	}
	
	/**
	 * Class whose instances represent service provisioning requests. 
	 * Requests are identified by an integer value representing the 
	 * service identifier.
	 */
	private static class Request
	{
		/**
		 * The service identifier
		 */
		public final int _serviceId;
		
		/**
		 * A boolean value indicating whether the service has been provisioned 
		 */
		public boolean _serviced;
		
		/**
		 * Holds the count of threads that share the request instance.
		 */
		public int _sharedByCount;
		
		/**
		 * Initiates an instance with the given service identifier.
		 * 
		 * @param serviceId
		 */
		public Request(int serviceId)
		{
			_serviceId = serviceId;
			_serviced = false;
			_sharedByCount = 1;
		}
	}
	
	/**
	 * Shared state that holds pending service provisioning requests, 
	 * sorted according to the corresponding service identifier.
	 * 
	 * Guarded by {@code _requests} intrinsic monitor
	 */
	private final List<Request> _requests;

	/**
	 * Helper method that gets the request associated to the received
	 * service identifier and increments the counter of threads sharing it. 
	 * 
	 * @param serviceId the service identifier
	 * @return the associated request or {@code null} if none exists
	 */
	private Request getServiceRequestAndIncrementSharedByCount(int serviceId) 
	{
		for(Request current : _requests)
		{
			if(current._serviceId == serviceId)
			{
				current._sharedByCount += 1;
				return current;
			}
		}
		return null;
	}
	
	/**
	 * Helper method that removes the request associated to the received
	 * service identifier.
	 * 
	 * @param serviceId the service identifier
	 * @return the removed request or {@code null} if none exists
	 */
	private Request removeServiceRequest(int serviceId) 
	{
		Iterator<Request> iterator = _requests.iterator();
		while(iterator.hasNext())
		{
			Request current = iterator.next();
			if(current._serviceId == serviceId)
			{
				iterator.remove();
				return current;
			}
		}
		return null;
	}

	/**
	 * Initiates a {@link ServiceProvisioningManagerDE} instance.
	 */
	public ServiceProvisioningManagerDE()
	{
		_requests = new LinkedList<>();
	}
	
	/**
	 * Requests the provisioning of the service with the given identifier.
	 * If there is no pending request for creation of a service with the given 
	 * identifier, the calling thread is used to trigger service creation.

	 * The caller is blocked until service provisioning completion is signaled, 
	 * regardless of the existence of a previous pending request for the received
	 * identifier. 
	 * 
	 * @param serviceId The service identifier
	 * @throws InterruptedException
	 */
	public void provisionService(int serviceId) throws InterruptedException
	{
		boolean triggerProvisioning = false;
		Request request = null;
		
		synchronized (_requests) 
		{
			// Check if service provisioning is ongoing
			request = getServiceRequestAndIncrementSharedByCount(serviceId);
			if(request == null)
			{
				// No! Create request and trigger provisioning
				request = new Request(serviceId);
				_requests.add(request);
				triggerProvisioning = true;
			}
		}
		
		if(triggerProvisioning)
			submitProvisioningActions(serviceId);

		synchronized (_requests) 
		{
			// Check if thread was signaled. This must be done first in order
			// to ensure that no notifications are lost.
			if(request._serviced)
				return;
			
			try 
			{
				while(true)
				{
					// Wait for completion
					_requests.wait();
					
					// Check if thread was signaled. 
					if(request._serviced)
						return;
				}
			}
			catch(InterruptedException ie)
			{
				// Decrement shared counter and remove request if it is 
				// the last thread
				if(--request._sharedByCount == 0)
					_requests.remove(request);
				throw ie;
			}
		}
	}
	
	/**
	 * Signals completion of service provisioning for the service with the given 
	 * identifier, unblocking all threads waiting for its completion.
	 * 
	 * @param serviceId the service identifier
	 * @throws IllegalStateException if the received serviced identifier has no
	 * corresponding request
	 */
	public void provisionServiceComplete(int serviceId) throws IllegalStateException
	{
		synchronized (_requests) 
		{
			// Remove request from the pending list
			Request request = removeServiceRequest(serviceId);
			if(request == null)
				throw new IllegalStateException();
			
			// Signal completion
			request._serviced = true;
			_requests.notifyAll();
		}
	}
}
