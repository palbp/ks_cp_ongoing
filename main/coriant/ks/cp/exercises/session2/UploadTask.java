package coriant.ks.cp.exercises.session2;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

import java.util.concurrent.*;

import coriant.ks.cp.utils.Functions;

/**
 * Class whose instances represent upload tasks. 
 * 
 * Although the implementation is based on the future task pattern, 
 * it has several limitations; it does not intend to be a full 
 * optimized implementation of a {@link java.util.concurrent.FutureTask}
 * 
 * <ul>This version's properties:
 * 		<li> task cancellation is not supported </li>
 * 		<li> task chaining is not supported (in continuations) </li> 
 * </ul>
 * 
 * @param <T> the upload operation's result type
 * 
 * Class instances are thread-safe.
 */
public final class UploadTask<T> implements Runnable
{
	/**
	 * Enumeration with the possible values for upload task status. 
	 */
	public static enum Status { PENDING, INPROGRESS, COMPLETED }
	
	/**
	 * Initiates an instance with the given arguments.
	 * 
	 * @param neId the request's NE identifier 
	 * @param type request's upload type
	 * @param action the task's action 
	 */
	public UploadTask(int neId, UploadType type, Callable<T> action)
	{
		// TODO
	}
	
	/**
	 * Gets the upload request's NE identifier.
	 * @return the request's NE identifier
	 */
	public int getNeId() 
	{
		// TODO
		return 0;
	}
	
	/**
	 * Gets the upload request's upload type.
	 * @return
	 */
	public UploadType getUploadType() 
	{ 
		// TODO
		return null;
	}
	
	/**
	 * Gets the request's upload type identifier.
	 * @return the request's upload type identifier
	 */
	public Status getStatus()
	{
		// TODO
		return null;
	}
	
	/**
	 * Gets the upload request's result, blocking the calling thread until the result 
	 * is available or the specified time interval {@code timeout} has elapsed.
	 *  
	 * @param timeout the number of milliseconds the calling thread is willing to wait 
	 * @return The upload result
	 * @throws ExecutionException if an error while performing the upload
	 * @throws InterruptedException if the calling thread was interrupted
	 * @throws TimeoutException if the specified timeout has elapsed
	 */
	public T getResult(long timeout) throws ExecutionException, TimeoutException, InterruptedException
	{
		// TODO
		return null;
	}
	
	/**
	 * Gets the upload request's result, blocking the calling thread until the result 
	 * is available.
	 *  
	 * @return The upload result
	 * @throws ExecutionException if an error while performing the upload
	 * @throws InterruptedException if the calling thread was interrupted
	 */
	public T getResult() throws ExecutionException, InterruptedException
	{
		// TODO
		return null;
	}
	
	/**
	 * Registers the given action to the list of actions that are executed 
	 * to after signaling completion.
	 * 
	 * @param action The action to be registered
	 * @return The current task instance, to enable fluent usage
	 */
	public UploadTask<T> whenDone(Functions.Action<UploadTask<T>> action) 
	{
		// TODO
		return null;
	}
	
	/**
	 * Registers the given action to the list of continuation actions. These
	 * actions are executed after the execution of all actions registered 
	 * with {@link whenDone}. 
	 * 
	 * @param action The action to be registered
	 * @return The current task instance, to enable fluent usage
	 */
	public UploadTask<T> continueWith(Functions.Action<UploadTask<T>> action) 
	{
		// TODO
		return null;
	}

	/**
	 * Method that contains the sequence of actions performed by task instances.
	 * This method is to be called by the thread that will execute the task's work. 
	 * 
	 * The sequence of actions is as follows:
	 * <ol>
	 * 	<li> Status is set to {@link Status.INPROGRESS} </li>
	 * 	<li> The task action is called and the result is stored </li>
	 * 	<li> Status is set to {@link Status.COMPLETED} </li>
	 *  <li> Registered when-done actions are executed </li>
	 * 	<li> Threads that are blocked waiting for the task's result are signaled </li>
	 *	<li> Continuation actions are executed </li>
	 * </ol>
	 */
	public void run()
	{
		// TODO
	}
}