package coriant.ks.cp.exercises.session2;
/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 2.
 * February 2014 
 * 
 * @author Paulo Pereira
 */

/**
 * Class that implements the upload execution scheduling policy, that is,
 * it insures that, for any given NE, there is at most one upload in progress.
 * 
 * @param <TResult> the upload operation's result type
 * 
 * Class instances are thread-safe.
 */
public final class UploadExecutionService<TResult> {

	/**
	 * Initiates an instance
	 */
	public UploadExecutionService()
	{
		// TODO
	}
	
	/**
	 * Method that adds the received upload task to the execution queue 
	 * if an upload for the same NE is not already being made.
	 * 
	 * @param futureRequest the upload task to be executed
	 * @return {@code true} if the upload request has been accepted, 
	 * {@code false} otherwise 
	 */
	public boolean offer(UploadTask<TResult> futureRequest)
	{
		// TODO
		return false;
	}
	
	/**
	 * Method that adds the received upload task to the execution queue. 
	 * 
	 * @param futureRequest the upload task to be executed
	 */
	public void put(UploadTask<TResult> futureRequest)
	{
		// TODO
	}
}
