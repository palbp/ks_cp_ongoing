/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 4.
 * March 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session4.solved;

import java.io.File;
import java.util.concurrent.*;

import coriant.ks.cp.utils.Functions.Aggregate;
import coriant.ks.cp.utils.Functions.Func;

/**
 * 
 * @author Paulo Pereira
 */
public final class FileSystemSearcher {

	/**
	 * Returns the number of files found recursively from the given root directory   
	 * 
	 * @param taskPool the task pool to be used
	 * @param rootPath the directory's full path name
	 * @return the file count
	 */
	private static int fileCount(ForkJoinPool taskPool, String rootPath) {
		return taskPool.invoke(new DirectoryTask<Integer>(
				new Func<Integer,File>() { 
					public Integer call(File leafFile) throws Exception
					{
						return 1;
					}
				}, 
				new Aggregate<Integer>() {
					public Integer call(Integer aggregated, Integer current)
					{
						int result = aggregated == null ? 0 : aggregated;
						result += current == null ? 0 : current;
						return result;
					}
				}, 
				new File(rootPath)
		));
	}
	
	/**
	 * 
	 * @param taskPool the task pool to be used
	 * @param rootPath the directory's full path name
	 * @return the java source file count
	 */
	private static int javaFileCount(ForkJoinPool taskPool, String rootPath)
	{
		return taskPool.invoke(new DirectoryTask<Integer>(
				new Func<Integer,File>() { 
					public Integer call(File leafFile) throws Exception
					{
						return leafFile.getName().endsWith(".java") ? 1 : 0;
					}
				}, 
				new Aggregate<Integer>() {
					public Integer call(Integer aggregated, Integer current)
					{
						int result = aggregated == null ? 0 : aggregated;
						result += current == null ? 0 : current;
						return result;
					}
				}, 
				new File(rootPath)
		));
	}

	/**
	 * Application entry point
	 * 
	 * @param args Command line arguments (ignored)
	 */
	public static void main(String[] args) throws InterruptedException
	{
		// Instantiate task pool to be used
		ForkJoinPool taskPool = new ForkJoinPool();
		
		// TODO: Define the path that will be used as the root of the file system search
		final String ROOT = null;
		
		System.out.println("File count from " + ROOT + " = " + fileCount(taskPool, ROOT));
		System.out.println("Java source file count from " + ROOT + " = " + javaFileCount(taskPool, ROOT));
		
		// Cleanup 
		taskPool.shutdown();
		taskPool.awaitTermination(2, TimeUnit.SECONDS);
	}
}
