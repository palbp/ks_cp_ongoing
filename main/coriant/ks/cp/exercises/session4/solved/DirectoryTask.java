/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 4.
 * March 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session4.solved;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

import coriant.ks.cp.utils.Functions.Aggregate;
import coriant.ks.cp.utils.Functions.Func;

/**
 * Class that represents the task to be performed in each directory.
 * 
 * @param <T> The task's result type
 */
@SuppressWarnings("serial") 
public class DirectoryTask<T> extends RecursiveTask<T> 
{
	/**
	 * The computation to be applied to each file.  
	 */
	private final Func<T, File> _leafFunc;
	
	/**
	 * The function to be used to aggregate partial results.
	 */
	private final Aggregate<T> _combineFunc;
	
	/**
	 * The target directory.
	 */
	private final File _target;
	
	/**
	 * Initiates a task instance with the specified computation, aggregate function
	 * and target directory.  
	 * 
	 * @param leafFunc The computation to be applied to each file
	 * @param aggregateFunc The function that aggregates partial results
	 * @param target The target directory 
	 */
	public DirectoryTask(Func<T, File> leafFunc, Aggregate<T> aggregateFunc, File target)
	{
		assert target.isDirectory();
		_leafFunc = leafFunc;
		_combineFunc = aggregateFunc;
		_target = target;
	}
	
	/**
	 * Computes the task's result
	 */
	@Override
	protected T compute() 
	{
		File[] childs = _target.listFiles();
		Collection<ForkJoinTask<T>> subTasks = new ArrayList<>(childs.length);
		
		// Spawn sub-tasks
		for(final File child : childs)
		{
			subTasks.add((child.isDirectory() ? 
					new DirectoryTask<T>(_leafFunc, _combineFunc, child).fork() :
					ForkJoinTask.adapt(new Callable<T>() {
						public T call() throws Exception { return _leafFunc.call(child); }
					}).fork())
			);
		}
		
		// Collect results
		T result = null;
		for(ForkJoinTask<T> subTask : subTasks)
			result = _combineFunc.call(result, subTask.join());
		return result;
	}
}