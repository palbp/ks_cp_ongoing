/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 4.
 * March 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session4;

import java.util.concurrent.RecursiveTask;

/**
 * Class that represents the task to be performed in each directory.
 * 
 * @param <T> The task's result type
 */
@SuppressWarnings("serial") 
public class DirectoryTask<T> extends RecursiveTask<T> 
{
	/**
	 * Computes the task's result
	 */
	@Override
	protected T compute() 
	{
		// TODO
		return null;
	}
}