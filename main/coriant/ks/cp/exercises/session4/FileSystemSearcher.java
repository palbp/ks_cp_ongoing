/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 4.
 * March 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session4;

import java.util.concurrent.*;

/**
 * Console application that performs several file system searches.
 */
public final class FileSystemSearcher {

	/**
	 * Returns the number of files found recursively from the given root directory.   
	 * 
	 * @param taskPool the task pool to be used
	 * @param rootPath the directory's full path name
	 * @return the file count
	 */
	private static int fileCount(ForkJoinPool taskPool, String rootPath) 
	{
		// TODO
		return 0;
	}
	
	/**
	 * Returns the number of java source files found recursively from the given 
	 * root directory.   
	 * 
	 * @param taskPool the task pool to be used
	 * @param rootPath the directory's full path name
	 * @return the java source file count
	 */
	private static int javaFileCount(ForkJoinPool taskPool, String rootPath)
	{
		// TODO
		return 0;
	}

	/**
	 * Application entry point
	 * 
	 * @param args Command line arguments (ignored)
	 */
	public static void main(String[] args) throws InterruptedException
	{
		// Instantiate task pool to be used
		ForkJoinPool taskPool = new ForkJoinPool();
		
		// TODO: Define the path that will be used as the root of the file system search
		final String ROOT = null;
		
		System.out.println("File count from " + ROOT + " = " + fileCount(taskPool, ROOT));
		System.out.println("Java source file count from " + ROOT + " = " + javaFileCount(taskPool, ROOT));
		
		// Cleanup 
		taskPool.shutdown();
		final int TIMEOUT = 2; 
		taskPool.awaitTermination(TIMEOUT, TimeUnit.SECONDS);
	}
}
