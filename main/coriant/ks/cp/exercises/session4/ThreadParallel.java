/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 4.
 * March 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class that contains the implementations of the parallel versions of several algorithms.
 * The implementations makes use of a traditional thread-pool executor. 
 */
@SuppressWarnings("unused")
public final class ThreadParallel {

	/**
	 * The number of processors in the target system.
	 */
	private static final int PROCESSOR_COUNT = Runtime.getRuntime().availableProcessors(); 
	
	/**
	 * The thread-pool executor instance.
	 */
	private static final ExecutorService _threadPool = Executors.
			newFixedThreadPool(PROCESSOR_COUNT);
	
	/**
	 * Adds the elements of the given array and produces the corresponding result.
	 * 
	 * @param array The array that contains the values
	 * @return The result
	 */
	public static long add(final long[] array)
	{
		// TODO
		return 0;
	}
}
