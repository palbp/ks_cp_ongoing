/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1;

/**
 * A non-optimized implementation of a simplified version of PM's UploadManager.
 */
public final class UploadManager
{
	/**
	 * Interface that specifies the contract to be supported by upload
	 * completion event listeners. 
	 */
	public static interface UploadCompletedListener
	{
		/**
		 * Called to notify upload completion.
		 *  
		 * @param neId The upload request identifier 
		 */
		public void notifyUploadCompleted(int neId);
	}
	
	
	/**
	 * Constant field that specifies the configured maximum number of simultaneous uploads.
	 */
	private static final int MAX_ONGOING_UPLOADS = 5;
	
	/**
	 * Singleton instance.
	 */
	private static final UploadManager _instance = new UploadManager(MAX_ONGOING_UPLOADS); 
	
	/**
	 * Gets the singleton instance.
	 * 
	 * @return the singleton instance 
	 */
	public static UploadManager getInstance()
	{
		return _instance;
	}
	
	
	/**
	 * Initiates an instance that supports at most {@link maxSimultaneousUploads} simultaneous
	 * uploads.
	 *   
	 * @param maxSimultaneousUploads the number of simultaneous uploads to be supported
	 */
	private UploadManager(int maxSimultaneousUploads)
	{
		// TODO
	}

	
	/**
	 * Creates an upload request for the given NE. The request is rejected if an upload request for the
	 * same NE is being performed or if it is pending. Upload completion is notified by calling the 
	 * given callback. 
	 *  
	 * @param neId The NE identifier, which is used to identify the upload request
	 * @param completionCallback The listener used to notify upload completion
	 * @return {@code true} if the upload request has been accepted, {@code false} otherwise
	 */
	public boolean performUpload(int neId, UploadCompletedListener completionCallback)
	{
		// TODO
		return false;
	}
}
