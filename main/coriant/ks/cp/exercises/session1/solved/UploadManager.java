/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1.solved;

import java.util.*;

/**
 * A non-optimized implementation of a simplified version of PM's UploadManager.
 * 
 * @author Paulo Pereira
 */
public final class UploadManager
{
	/**
	 * Interface that specifies the contract to be supported by upload
	 * completion event listeners. 
	 */
	public static interface UploadCompletedListener
	{
		/**
		 * Called to notify upload completion.
		 *  
		 * @param neId The upload request identifier 
		 */
		public void notifyUploadCompleted(int neId);
	}
	
	/**
	 * Internal class whose instances represent upload requests.
	 */
	private static final class UploadRequest
	{
		public final int _neId;
		public final UploadCompletedListener _callback;
		
		public UploadRequest(int neId, UploadCompletedListener callback) 
		{ 
			_neId = neId; _callback = callback; 
		}   
	}
	
	/**
	 * Constant field that specifies the maximum number of simultaneous uploads.
	 */
	private static final int MAX_ONGOING_UPLOADS = 5;
	
	/**
	 * Singleton instance.
	 */
	private static final UploadManager _instance = new UploadManager(MAX_ONGOING_UPLOADS); 
	
	/**
	 * Gets the singleton instance.
	 * 
	 * @return the singleton instance 
	 */
	public static UploadManager getInstance()
	{
		return _instance;
	}
	
	/**
	 * Contains the maximum count of allowed simultaneous uploads, specified upon instantiation.  
	 */
	private final int _maxUploads;
	
	/**
	 * Queue of pending upload requests. 
	 * Shared state guarded by {@link _pendingUploads} intrinsic's lock.
	 */
	private final Queue<UploadRequest> _pendingUploads;
	
	/**
	 * List of ongoing uploads. 
	 * Shared state guarded by {@link _pendingUploads} intrinsic's lock.
	 */
	private final List<UploadRequest> _ongoingUploads;
	
	/**
	 * Initiates an instance that supports at most {@code maxSimultaneousUploads} simultaneous
	 * uploads.  
	 * 
	 * @param maxSimultaneousUploads the maximum number of simultaneous uploads to be supported
	 */
	private UploadManager(int maxSimultaneousUploads)
	{
		_maxUploads = maxSimultaneousUploads;
		_pendingUploads = new LinkedList<>();
		_ongoingUploads = new LinkedList<>();
	}

	/**
	 * Starts the upload identified by the given request.
	 * 
	 * @param request the upload request to be started
	 */
	private void startUpload(final UploadRequest request)
	{
		new Thread() {
			
			private void doUpload(final UploadRequest request)
			{
				// Perform the upload (simulated) by using the provided information
				try {
					// Simulate upload lasting 2 seconds, although it should take 
					// much longer in the envisioned scenario
					Thread.sleep(2*1000);  
				}
				catch(InterruptedException unexpected) { }
			}
			
			public void run()
			{
				UploadRequest currentRequest = request;
				while(true)
				{
					doUpload(currentRequest);
					
					synchronized (_pendingUploads)
					{
						// Remove request from ongoing list
						_ongoingUploads.remove(currentRequest);

						// Notify completion. 
						// Notice that there is a possible race here! An upload request is unaccounted  
						// for and the completion callback has not yet been called, hence the lock ownership.
						currentRequest._callback.notifyUploadCompleted(currentRequest._neId);

						// Dequeue next pending request
						currentRequest = _pendingUploads.poll();
						// If there is no pending work, terminate thread.
						if(currentRequest == null)
							return;
						// Add next request to ongoing upload list
						_ongoingUploads.add(currentRequest);
					}
				}
			}
		}.start();
	}
	
	/**
	 * Creates an upload request for the given NE. The request is rejected if an upload request for the
	 * same NE is being performed or if it is pending. Upload completion is notified by calling the 
	 * given callback. 
	 *  
	 * @param neId The NE identifier, which is used to identify the upload request
	 * @param completionCallback The listener used to notify upload completion
	 * @return {@code true} if the upload request has been accepted, {@code false} otherwise
	 */
	public boolean performUpload(int neId, UploadCompletedListener completionCallback)
	{
		// If not pending or ongoing, submit work item for execution
		synchronized(_pendingUploads)
		{
			// Check pending
			for(UploadRequest current : _pendingUploads)
				if(current._neId == neId)
					return false;

			// Check ongoing
			for(UploadRequest current : _ongoingUploads)
				if(current._neId == neId)
					return false;

			// Not found. Queue it or start it.
			UploadRequest request = new UploadRequest(neId, completionCallback);
			if(_ongoingUploads.size() == _maxUploads)
				_pendingUploads.add(request);
			else
			{
				_ongoingUploads.add(request);
				startUpload(request);
			}
			return true;
		}
	}
}
