/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1.solved;

import java.awt.EventQueue;

/**
 * Class that implements a background worker. The class design mandates that each instance is only used once. 
 *  
 * The class provides a pull interface, meaning, the data regarding the computation
 * is pulled by calling the {@link get} method. This decision implies that viable solutions
 * must resort to synchronization based techniques.
 * 
 * <ul>This version's limitations:
 * 		<li> The {@link #get} method does not support timeout. </li>
 *  
 * 		<li> Open call to {@link #doInBackground} to ensure that the 
 * 		{@link #get} method blocks until completion. </li>
 * 
 * 		<li> Not a viable solution because computation status may only 
 * 		be accessed after completion (i.e. an open call is used to block 
 * 		threads that call the {@link #get()} method. </li>
 *  
 * 		<li> Background work cannot be canceled.</li>
 * </ul>
 * 
 * @author Paulo Pereira
 */
public abstract class SimplePullBackgroundWorker<Result> 
{
	/**
	 * The intrinsic lock used internally for synchronization.
	 * Used to guard accesses to shared data: {@link #_result}; {@link #_exception}.
	 */
	private final Object _privateLock;
	
	/**
	 * Shared field that holds the computed result.
	 * Guarded by {@link #_privateLock}
	 */
	private Result _result;
	
	/**
	 * Shared field that holds the exception thrown by the computation, if any.
	 * Guarded by {@link #_privateLock}
	 */
	private Exception _exception;
	
	/**
	 * Initiates the new instance.
	 */
	public SimplePullBackgroundWorker()
	{
		_privateLock = new Object();
		_result = null;
		_exception = null;
	}
	
	/**
	 * Triggers the background computation defined by the {@link doInBackground} method. 
	 */
	public final void execute()
	{
		// Unbounded thread creation: not a good idea!
		new Thread() {
			@Override
			public void run()
			{
				// Compute and publish result. An open call is used, thus ensuring that 
				// callers of the get method are blocked until completion
				synchronized(_privateLock)
				{
					try { _result = doInBackground(); }
					catch(Exception exc) { _exception = exc; }

					// Notify completion
					EventQueue.invokeLater(new Runnable() {
						@Override
						public void run() { done(); }
					});
				}
			}
		}.start();
	}
	
	/**
	 * Waits, if necessary, for the computation to complete, and then retrieves its result.
	 *  
	 * @return the computed result
	 * @throws Exception if the computation threw an exception
	 */
	public final Result get() throws Exception
	{
		synchronized(_privateLock)
		{
			if(_exception != null)
				throw _exception;
			return _result;
		}
	}
	
	/**
	 * Executes in a background thread. This method must be overridden in order to define the computation
	 * to be performed in background. 
	 * 
	 * @return the computed result
	 * @throws Exception if the computation fails
	 */
	protected abstract Result doInBackground() throws Exception;
	
	/**
	 * Executes in the Event Dispatcher thread. Called upon computation completion. 
	 */
	protected abstract void done();
	
}
