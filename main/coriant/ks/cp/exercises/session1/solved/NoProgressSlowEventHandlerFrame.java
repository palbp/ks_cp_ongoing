/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1.solved;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


@SuppressWarnings("serial")
public final class NoProgressSlowEventHandlerFrame extends JFrame {

	/**
	 * UI controls
	 */
	private final JLabel _message;
	private final JButton _start;
	
	/**
	 * Text pertaining to the presented messages. 
	 */
	private static final String IDLE_MSG = "Idle", WORK_MSG = "Working...", DONE_MSG = "Done!";
	
	/**
	 * Starts the background work using an instance of {@link SimplePullBackgroundWorker}
	 */
	private void startSimplePullBackgroundWorker()
	{
		new SimplePullBackgroundWorker<String>() {
			@Override
			public String doInBackground() throws Exception
			{
				boolean simulateFailure = (int) (Math.random() * 100) % 2 != 0;
				// Doing something slow...
				for(int i=0; i<20; ++i)
				{
					if(i == 5 && simulateFailure)
						throw new Exception("Error!");
					try { Thread.sleep(500); }
					catch(InterruptedException ignored) { /* Not cancelable */ }
				}
				return DONE_MSG;
			}
			
			@Override
			public void done()
			{
				// Completed
				String text = null;
				try {
					text = get();
				}
				catch(Exception e)
				{
					text = e.getMessage();
				}
				finally {
					_message.setText(text);
					_start.setEnabled(true);
				}
			}
		}.execute();
	}
	
	/**
	 * Sets the frame's behavior (i.e. reactions to UI events)
	 */
	private void initBehavior()
	{
		_start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				// Setup
				_start.setEnabled(false);
				_message.setText(WORK_MSG);
				
				// Schedule work
				startSimplePullBackgroundWorker();
			}
		});
	}
	
	/**
	 * Initiates an instance.
	 */
	public NoProgressSlowEventHandlerFrame()
	{
		super("Slow handler demo");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(_message = new JLabel(IDLE_MSG, JLabel.CENTER), BorderLayout.NORTH);
		final JPanel southPanel = new JPanel();
		southPanel.add(_start = new JButton("Start")); 
		add(southPanel, BorderLayout.SOUTH);
		pack();
		setSize(getWidth()*2, getHeight());
	}
	
	/**
	 * Shows the current instance and enables it's behavior.
	 * (note: designed to prevent "this escape")
	 */
	public void showWindow()
	{
		if(isVisible()) return;
		initBehavior();
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(); 
		setLocation((screen.width-getWidth())/2, (screen.height-getHeight())/2);
		setVisible(true);
	}
	
	/**
	 * Application entry point
	 * 
	 * @param args
	 */
	public static void main(String[] args) 
	{
		new NoProgressSlowEventHandlerFrame().showWindow();
	}
}
