/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1.solved;

import java.awt.EventQueue;

/**
 * Class that implements a background worker. The class design mandates that each 
 * instance is only used once. 
 *  
 * The class provides a push interface, meaning, the data regarding the computation
 * is pushed to the callback methods (as arguments). This decision enables solutions
 * that do not resort to synchronization based techniques.
 *     
 * <ul>This version's properties:
 * 		<li> Background work progress and completion are exclusively reported through 
 * 		callback's arguments</li>
 * 		<li> Background work does not report failures</li>
 * 		<li> Background work cannot be canceled</li>
 * </ul>
 * 
 * Note: This is merely a possible solution to the proposed challenge.
 * 
 * @author Paulo Pereira
 */
public abstract class PushBackgroundWorkerNoException<Result> 
{
	/**
	 * Triggers the background computation defined by the {@link doInBackground} method. 
	 */
	public final void execute()
	{
		// Unbounded thread creation: not a good idea!
		new Thread() {
			@Override
			public void run()
			{
				// Compute result
				final Result result = doInBackground(); 
				// Notify completion
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() { done(result); }
				});
			}
		}.start();
	}
	
	/**
	 * Called to publish progress changes. 
	 * 
	 * @param progress The current progress, in percentage of completed work 
	 * @throws IllegalArgumentException if {@link progress} is not within the 
	 * specified interval (i.e. [0..100] 
	 */
	protected final void setProgress(final int progress)
	{
		if(progress < 0 || progress > 100)
			throw new IllegalArgumentException();
		
		// Notify progress change
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() { reportProgress(progress); }
		});
	}
	
	/**
	 * Executes in a background thread. This method must be overridden in order to define the computation
	 * to be performed in background. 
	 * 
	 * @return the computed result
	 */
	protected abstract Result doInBackground();
	
	/**
	 * Executes in the Event Dispatcher thread. Called to notify computation progress changes.
	 *  
	 * @param progress The current percentage of completed work
	 */
	protected void reportProgress(int progress)	{ /* empty */ }

	/**
	 * Executes in the Event Dispatcher thread. Called upon computation completion.
	 *  
	 * @param result The computation's result
	 */
	protected abstract void done(Result result);
}
