/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1.solved;

import java.util.*;

/**
 * A simplistic thread-safe cache implementation that stores (key,value) pairs. 
 * 
 * <ul>This version's properties:
 * 		<li> The value associated to a given key is computed exactly once, upon 
 * 		first retrieval </li>
 * 		<li> The implementation does not include a purge policy, meaning, it is 
 * 		unbounded </li> 
 * 		<li> {@code null} values and {@code null} keys are not allowed </li>
 * </ul>
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
public final class SimpleCache<TKey,TValue> {

	/**
	 * Interface that represents key to value mapping functions.
	 * 
	 * @param <K> The key's type
	 * @param <V> The value's type
	 */
	public static interface Func<K,V> {
		/**
		 * Returns the value associated to the given key.
		 * 
		 * @param key The key
		 * @return The associated value
		 */
		public V call(K key);
	}
	
	/**
	 * Class whose instances represent a future value associated to a given key. 
	 * The value is computed on the first call to {@link get}.    
	 */
	private class Future
	{
		/**
		 * Holds the pair's key
		 */
		private final TKey _key;
		
		/**
		 * Holds the pair's value or {@code null} if it has not yet been computed
		 */
		private TValue _value;
		
		/**
		 * Initiates an instance with the given key.
		 * 
		 * @param key The item's key
		 */
		public Future(TKey key)
		{
			_key = key;
			_value = null;
		}
		
		/**
		 * Returns the item's value, computing it if necessary.
		 * The intrinsic lock of this instance is used to block callers until the 
		 * item's value is computed.
		 * 
		 * @return The item's value
		 */
		public synchronized TValue get()
		{
			if(_value == null)
				_value = _mapFunction.call(_key);
			return _value;
		}
	}

	/**
	 * Holds the key to value mapping function used by this cache instance. 
	 */
	private final Func<TKey,TValue> _mapFunction; 
	
	/**
	 * Shared state that holds the cached (key,value) pairs.
	 * 
	 * It's intrinsic lock is used to guard accesses to shared state.  
	 */
	private final Map<TKey,Future> _cache;
	
	/**
	 * Initiates an instance with the provided key to value mapping function.
	 * 
	 * @param mapFunction The function that produces the value associated to a given key
	 */
	public SimpleCache(Func<TKey,TValue> mapFunction)
	{
		_mapFunction = mapFunction;
		_cache = new HashMap<>();
	}
	
	/**
	 * Returns the value associated to the given key, computing it if it has not yet 
	 * been computed.
	 * If the value is not yet available, that is, it is being computed, the caller 
	 * is blocked until the computation is completed.
	 * 
	 * @param key the key
	 * @return the value produced for the given key
	 */
	public TValue get(TKey key)
	{
		Future futureValue = null;
		synchronized (_cache) 
		{
			if(!_cache.containsKey(key))
				_cache.put(key, futureValue = new Future(key));
			else 
				futureValue = _cache.get(key);
		}
		return futureValue.get();
	}
}
