/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1.solved;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public final class SomeSlowEventHandlerFrame extends JFrame {

	/**
	 * UI controls
	 */
	private final JLabel _message;
	private final JProgressBar _progress;
	private final JButton _start, _cancel;
	
	/**
	 * Text pertaining to the presented messages. 
	 */
	private static final String IDLE_MSG = "Idle", WORK_MSG = "Working...", DONE_MSG = "Done!";
	
	/**
	 * Starts the background work using an instance of {@link PushBackgroundWorker}
	 */
	private void startWorker()
	{
		new PushBackgroundWorker<String>() {
			@Override
			public String doInBackground() throws Exception
			{
				// Doing something slow...
				for(int i=0; i<20; ++i)
				{
					setProgress(i*5);
					Thread.sleep(500); 
				}
				setProgress(100);
				return DONE_MSG;
			}
			
			@Override
			protected void reportProgress(int percentage)
			{
				_progress.setValue(percentage);
			}

			@Override
			protected void done(PushBackgroundWorkerResult<String> result) 
			{
				// Completed
				try { 
					_message.setText(result.get());
				} 
				catch (Exception e) {
					setProgress(0);
					_message.setText(IDLE_MSG);
				} 
				finally {
					_start.setEnabled(true);
					_cancel.setEnabled(false);
				}
			}
		}.execute();
	}
	
	/**
	 * Sets the frame's behavior (i.e. reactions to UI events)
	 */
	private void initBehavior()
	{
		_start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				// Setup
				_start.setEnabled(false);
				_cancel.setEnabled(true);
				_progress.setValue(0);
				_message.setText(WORK_MSG);
				
				// Schedule work
				startWorker();
			}
		});
	}
	
	/**
	 * Initiates an instance.
	 */
	public SomeSlowEventHandlerFrame()
	{
		super("Slow handler demo");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(_message = new JLabel(IDLE_MSG, JLabel.CENTER), BorderLayout.NORTH);
		add(_progress = new JProgressBar(), BorderLayout.CENTER);
		final JPanel southPanel = new JPanel();
		southPanel.add(_start = new JButton("Start")); 
		southPanel.add(_cancel = new JButton("Cancel")); 
		_cancel.setEnabled(false);
		add(southPanel, BorderLayout.SOUTH);
		pack();
		setSize(getWidth()*2, getHeight());
	}
	
	/**
	 * Shows the current instance and enables it's behavior.
	 * (note: designed to prevent "this escape")
	 */
	public void showWindow()
	{
		if(isVisible()) return;
		initBehavior();
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(); 
		setLocation((screen.width-getWidth())/2, (screen.height-getHeight())/2);
		setVisible(true);
	}
	
	/**
	 * Application entry point
	 * 
	 * @param args
	 */
	public static void main(String[] args) 
	{
		new SomeSlowEventHandlerFrame().showWindow();
	}
}
