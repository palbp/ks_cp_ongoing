/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Implementations of the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1.solved;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


@SuppressWarnings("serial")
public final class SlowEventHandlerFrame extends JFrame {

	/**
	 * UI controls
	 */
	private final JLabel _message;
	private final JProgressBar _progress;
	private final JButton _start;
	
	/**
	 * Text pertaining to the presented messages. 
	 */
	private static final String IDLE_MSG = "Idle", WORK_MSG = "Working...", DONE_MSG = "Done!";
	
	/**
	 * Constant used to switch the background worker implementation in use 
	 */
	private static final boolean ENABLE_NO_EXCEPTION_BW = false;
	
	/**
	 * Starts the background work using an instance of {@link PushBackgroundWorkerNoException}
	 */
	private void startPushBackgroundWorkerNoException()
	{
		new PushBackgroundWorkerNoException<String>() {
			@Override
			public String doInBackground()
			{
				// Doing something slow...
				for(int i=0; i<20; ++i)
				{
					setProgress(i*5);
					try { Thread.sleep(500); }
					catch(InterruptedException ignored) { /* Not cancelable */ }
				}
				setProgress(100);
				return DONE_MSG;
			}
			
			@Override
			public void done(String result)
			{
				_message.setText(result);
				_start.setEnabled(true);
			}

			@Override
			protected void reportProgress(int percentage)
			{
				_progress.setValue(percentage);
			}
		}.execute();
	}
	
	/**
	 * Starts the background work using an instance of {@link PushBackgroundWorker}
	 */
	private void startPushBackgroundWorker()
	{
		new PushBackgroundWorker<String>() {
			@Override
			public String doInBackground() throws Exception
			{
				boolean simulateFailure = (int) (Math.random() * 100) % 2 != 0;
				// Doing something slow...
				for(int i=0; i<20; ++i)
				{
					setProgress(i*5);
					if(i == 5 && simulateFailure)
						throw new Exception("Error!");
					try { Thread.sleep(500); }
					catch(InterruptedException ignored) { /* Not cancelable */ }
				}
				setProgress(100);
				return DONE_MSG;
			}
			
			@Override
			public void done(PushBackgroundWorkerResult<String> result)
			{
				String text = null;
				try {
					text = result.get();
				}
				catch(Exception e)
				{
					text = e.getMessage();
				}
				finally {
					_message.setText(text);
					_start.setEnabled(true);
				}
			}

			@Override
			protected void reportProgress(int percentage)
			{
				_progress.setValue(percentage);
			}
		}.execute();

	}
	
	/**
	 * Sets the frame's behavior (i.e. reactions to UI events)
	 */
	private void initBehavior()
	{
		_start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				// Setup
				_start.setEnabled(false);
				_progress.setValue(0);
				_message.setText(WORK_MSG);
				
				// Schedule work
				if(ENABLE_NO_EXCEPTION_BW)
					startPushBackgroundWorkerNoException();
				else
					startPushBackgroundWorker();
			}
		});
	}
	
	/**
	 * Initiates an instance.
	 */
	public SlowEventHandlerFrame()
	{
		super("Slow handler demo");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(_message = new JLabel(IDLE_MSG, JLabel.CENTER), BorderLayout.NORTH);
		add(_progress = new JProgressBar(), BorderLayout.CENTER);
		JPanel southPanel = new JPanel();
		southPanel.add(_start = new JButton("Start"));
		add(southPanel, BorderLayout.SOUTH);
		pack();
		setSize(getWidth()*2, getHeight());
	}
	
	/**
	 * Shows the current instance and enables it's behavior.
	 * (note: designed to prevent "this escape")
	 */
	public void showWindow()
	{
		if(isVisible()) return;
		initBehavior();
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(); 
		setLocation((screen.width-getWidth())/2, (screen.height-getHeight())/2);
		setVisible(true);
	}
	
	/**
	 * Application entry point
	 * 
	 * @param args
	 */
	public static void main(String[] args) 
	{
		new SlowEventHandlerFrame().showWindow();
	}
}
