/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1;

/**
 * Class that implements a background worker. The class design mandates that each instance is only used once. 
 *  
 * The class provides a pull interface, meaning, the data regarding the computation
 * is pulled by calling the {@link get} method. This decision implies that viable solutions
 * must resort to synchronization based techniques.
 * 
 * <ul>This version's properties:
 * 		<li> The {@link #get} method does not support timeout </li>
 *  
 * 		<li> Open call to {@link #doInBackground} to ensure that the 
 * 		{@link #get} method blocks until completion </li>
 * 
 * 		<li> Not a viable solution because computation status may only 
 * 		be accessed after completion (i.e. an open call is used to block 
 * 		threads that call the {@link #get()} method </li>
 *  
 * 		<li> Background work cannot be canceled.</li>
 * </ul>
 * 
 * @param Result The background work result type
 */
public abstract class SimplePullBackgroundWorker<Result> 
{
	/**
	 * Initiates the new instance.
	 */
	public SimplePullBackgroundWorker()
	{
		// TODO
	}
	
	/**
	 * Triggers the background computation defined by the {@link doInBackground} method. 
	 */
	public final void execute()
	{
		// TODO
	}
	
	/**
	 * Waits, if necessary, for the computation to complete, and then retrieves its result.
	 *  
	 * @return the computed result
	 * @throws Exception if the computation threw an exception
	 */
	public final Result get() throws Exception
	{
		// TODO
		return null;
	}
	
	/**
	 * Executes in a background thread. This method must be overridden in order to define the computation
	 * to be performed in background. 
	 * 
	 * @return the computed result
	 * @throws Exception if the computation fails
	 */
	protected abstract Result doInBackground() throws Exception;
	
	/**
	 * Executes in the Event Dispatcher thread. Called upon computation completion. 
	 */
	protected abstract void done();
	
}
