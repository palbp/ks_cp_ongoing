/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Skeleton implementations to be used to address the challenges proposed in Session 1.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.exercises.session1;

import java.util.*;

/**
 * A simplistic thread-safe cache implementation that stores (key,value) pairs. 
 * 
 * <ul>This version's properties:
 * 		<li> The value associated to a given key is computed exactly once, upon 
 * 		first retrieval </li>
 * 		<li> The implementation does not include a purge policy, meaning, it is 
 * 		unbounded </li> 
 * 		<li> {@code null} values and {@code null} keys are not allowed </li>
 * </ul>
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
public final class SimpleCache<TKey,TValue> {

	/**
	 * Interface that represents key to value mapping functions.
	 * 
	 * @param <K> The key's type
	 * @param <V> The value's type
	 */
	public static interface Func<K,V> {
		
		/**
		 * Returns the value associated to the given key.
		 * 
		 * @param key The key
		 * @return The associated value
		 */
		public V call(K key);
	}
	
	private final Map<TKey,TValue> _cache;
	private final Func<TKey,TValue> _mapFunction; 
	
	/**
	 * Initiates an instance with the provided key to value mapping function.
	 * 
	 * @param mapFunction The function that produces the value associated to a given key
	 */
	public SimpleCache(Func<TKey,TValue> mapFunction)
	{
		_mapFunction = mapFunction;
		_cache = new HashMap<TKey,TValue>();
	}
	
	/**
	 * Returns the value associated to the given key, computing it if it has not yet been computed.
	 * If the value is not yet available, that is, it is being computed, the caller is blocked
	 * until the computation is completed.
	 * 
	 * @param key the key
	 * @return the value produced for the given key
	 */
	public TValue get(TKey key)
	{
		synchronized(_cache)
		{
			TValue value;
			if(!_cache.containsKey(key))
				_cache.put(key, value = _mapFunction.call(key));
			else
				value = _cache.get(key);
			return value;
		}
	}
}
