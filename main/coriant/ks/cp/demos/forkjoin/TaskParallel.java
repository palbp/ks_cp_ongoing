/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos to be used in Session 4.
 * March 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.forkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

import coriant.ks.cp.utils.Functions.Action;

/**
 * Class that contains the implementations of the parallel versions of several algorithms.
 * The implementations are supported on the fork-join framework. 
 */
@SuppressWarnings("serial")
public final class TaskParallel {

	/**
	 * The task executor instance.
	 */
	private static final ForkJoinPool _taskPool = new ForkJoinPool();
	
	/**
	 * A simplistic implementation of a generalized parallel for. 
	 * 
	 * @param startIdx The starting iteration number
	 * @param endIdx The ending iteration count
	 * @param body The for's body
	 */
	public static void forRange(int startIdx, int endIdx, Action<Integer> body)
	{
		class Iteration extends RecursiveAction {
			private final int _start, _end;
			private final Action<Integer> _body;
			
			private static final int THRESHOLD = 8098;
			
			public Iteration(int start, int end, final Action<Integer> body)
			{
				_start = start; _end = end;	_body = body;
			}
			
			@Override
			protected void compute() 
			{
				int middle = ((_end - _start) / 2);
				if(middle < THRESHOLD)
				{
					Sequential.forRange(_start, _end, _body);
				}
				else {
					invokeAll(
							new Iteration(_start, middle + _start, _body), 
							new Iteration(middle + _start, _end, _body)
							);
				}
			}
		};
		_taskPool.invoke(new Iteration(startIdx, endIdx, body));
	}

	/**
	 * Returns the maximum element in the given array.
	 * 
	 * @param array The array containing the elements to consider
	 * @return The largest element  
	 */
	public static <T extends Comparable<T>> T max(T[] array)
	{
		class MaxTask extends RecursiveTask<T> {

			private final T[] _array;
			private final int _startIdx;
			private final int _endIdx;
			
			private static final int THRESHOLD = 1 << 8;
			
			private T sequentialMax() 
			{
				T currentMax = _array[_startIdx];
				for(int i = _startIdx+1; i < _endIdx; ++i)
					if(currentMax.compareTo(_array[i]) < 0)
						currentMax = _array[i];
				
				return currentMax;
			}
			
			public MaxTask(T[] array, int start, int end)
			{
				_array = array;
				_startIdx = start;
				_endIdx = end;
			}
			
			@Override
			protected T compute() 
			{
				// if(_startIdx == _endIdx)
				//    return _array[_startIdx];
				if(_endIdx - _startIdx < THRESHOLD)
					return sequentialMax();
				
				int middleIdx = ((_endIdx - _startIdx) / 2) + _startIdx;
				// T leftMax = InternalMax(_array, _startIdx, middleIdx);
				MaxTask left = new MaxTask(_array, _startIdx, middleIdx);
				left.fork();
				// T rightMax = InternalMax(_array, middleIdx, _endIdx);
				MaxTask right = new MaxTask(_array, middleIdx, _endIdx);
				right.fork();
				
				// return leftMax.compareTo(rightMax) > 0 ? leftMax : rightMax;
				T leftResult = left.join();
				T rightResult = right.join();
				return leftResult.compareTo(rightResult) > 0 ? leftResult : rightResult;
			}
		};
		
		return _taskPool.invoke(new MaxTask(array, 0, array.length));
	}
}
