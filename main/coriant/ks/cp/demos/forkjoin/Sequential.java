/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos to be used in Session 4.
 * March 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.forkjoin;

import java.lang.reflect.Array;
import java.util.Arrays;

import coriant.ks.cp.utils.Functions.Action;

/**
 * Class that contains the implementations of the sequential versions of several algorithms.
 */
public final class Sequential {

	/**
	 * A simplistic implementation of a generalized for. 
	 * 
	 * @param startIdx The starting iteration number
	 * @param endIdx The ending iteration count
	 * @param body The for's body
	 */
	public static void forRange(final int startIdx, final int endIdx, final Action<Integer> body)
	{
		for(int i=startIdx; i<endIdx; ++i)
		{
			final int currIdx = i;
			body.call(currIdx);
		}
	}

	/**
	 * A simplistic implementation of a generalized for-each
	 * 
	 * @param sequence The input sequence
	 * @param body The for-each's body
	 */
	public static <T> void forEach(Iterable<T> sequence, Action<T> body)
	{
		for(T elem : sequence)
			body.call(elem);
	}

	/**
	 * Returns the maximum element in the given array.
	 * 
	 * @param array The array containing the elements to consider
	 * @return The largest element  
	 */
	public static <T extends Comparable<T>> T iterativeMax(T[] array)
	{
		T currentMax = null;
		for(T elem : array)
			if(currentMax == null || currentMax.compareTo(elem) < 0)
				currentMax = elem;
		
		return currentMax;
	}

	/**
	 * Helper method that returns the maximum element found in the given array within the specified bounds.
	 *  
	 * @param array The array containing the elements to consider
	 * @param startIdx The array lower bound (inclusive)
	 * @param endIdx The array upper bound (exclusive)
	 * @return The largest element  
	 */
	private static <T extends Comparable<T>> T internalMax(T[] array, int startIdx, int endIdx)
	{
		if(startIdx == endIdx)
			return array[startIdx];
		
		int middleIdx = ((endIdx - startIdx) / 2) + startIdx;
		T leftMax = internalMax(array, startIdx, middleIdx);
		T rightMax = internalMax(array, middleIdx, endIdx);
		
		return leftMax.compareTo(rightMax) > 0 ? leftMax : rightMax;
	}
	
	/**
	 * Returns the maximum element in the given array.
	 * 
	 * @param array The array containing the elements to consider
	 * @return The largest element  
	 */
	public static <T extends Comparable<T>> T recursiveMax(T[] array)
	{
		return internalMax(array, 0, array.length);
	}

	/**
	 * Helper method that checks if the specified bounds of the given array are within the algorithm's 
	 * threshold and, if they are, sorts the array.
	 * 
	 * @param array The input array
	 * @param startIdx the array's lower bound (inclusive)
	 * @param endIdx the array's upper bound (exclusive)
	 * @return {@code true} if the array was sorted, {@code false} otherwise 
	 */
	private static <T extends Comparable<T>> boolean checkSortThreshold(T[] array, int startIdx, int endIdx)
	{
		final int THRESHOLD = 1 << 8;
		if(endIdx - startIdx < THRESHOLD)
		{
			Arrays.sort(array, startIdx, endIdx);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Helper method that merges the two sections of the given array using the provided temporary array.
	 * 
	 * @param array The array containing the sections to be merged
	 * @param temp The temporary array
	 * @param startIdx The lower bound of the first array section (inclusive)
	 * @param middleIdx The upper bound of the first array section (exclusive) and lower bound of 
	 * the second array section (inclusive)
	 * @param endIdx The upper bound of the second array section (exclusive)
	 */
	private static <T extends Comparable<T>> void merge(T[] array, T[] temp, int startIdx, int middleIdx, int endIdx)
	{
		// TODO:
	}
	
	/**
	 * Helper method that sorts the elements within the specified bounds of the given {@code array}.   
	 * 
	 * @param array The array to be sorted
	 * @param temp An auxiliary array to be used in the sorting algorithm
	 * @param startIdx the array's lower bound (inclusive)
	 * @param endIdx the array's upper bound (exclusive)
	 */
	private static <T extends Comparable<T>> void internalMergeSort(T[] array, T[] temp, int startIdx, int endIdx)
	{
		if(checkSortThreshold(array, startIdx, endIdx))
			return;
		
		int middleIdx = ((endIdx - startIdx) / 2) + startIdx;
		internalMergeSort(array, temp, startIdx, middleIdx);
		internalMergeSort(array, temp, middleIdx, endIdx);
		
		merge(array, temp, startIdx, middleIdx, endIdx);
	}
	
	/**
	 * Sorts the given array (ascending order).
	 * 
	 * @param array The array to be sorted
	 */
	public static <T extends Comparable<T>> void sort(T[] array)
	{
		if(checkSortThreshold(array, 0, array.length))
			return;
		
		@SuppressWarnings("unchecked")
		T[] tmp = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
		internalMergeSort(array, tmp, 0, array.length);
	}
}
