/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos to be used in Session 4.
 * March 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.forkjoin;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

import coriant.ks.cp.utils.Functions.Action;

/**
 * Class that contains the implementations of the parallel versions of several algorithms.
 * The implementations make use of a traditional thread-pool executor. 
 */
public final class ThreadParallel {

	/**
	 * The number of processors in the target system.
	 */
	private static final int PROCESSOR_COUNT = Runtime.getRuntime().availableProcessors(); 
	
	/**
	 * The thread-pool executor instance.
	 */
	private static final ExecutorService _threadPool = Executors.
			newFixedThreadPool(PROCESSOR_COUNT);
	
	/**
	 * Adds the elements of the given array and produces the corresponding result.
	 * The implementation makes use of the Future instances produced by the thread pool.
	 * 
	 * @param array The array that contains the values
	 * @return The result
	 */
	public static long addWithFuture(final long[] array)
	{
		// Define computation work unit
		class PartitionProcessor implements Callable<Long>
		{
			private final int _startIdx, _endIdx;
			private final long[] _array;
			
			public PartitionProcessor(long[] array, int start, int end)
			{
				_array = array;
				_startIdx = start;
				_endIdx = end;
			}
			
			@Override
			public Long call() throws Exception
			{
				long partialResult = 0;
				for(int idx = _startIdx; idx < _endIdx; ++idx)
					partialResult += _array[idx];
				
				return partialResult;
			}
		}
		
		// Assign work
		final int blockSize = array.length / PROCESSOR_COUNT;
		Collection<Future<Long>> results = new ArrayList<Future<Long>>(PROCESSOR_COUNT);
		for(int partitionCount = 0; partitionCount < PROCESSOR_COUNT; ++partitionCount)
		{
			int blockStart = partitionCount * blockSize;
			int blockEnd = partitionCount != PROCESSOR_COUNT-1 ? blockStart + blockSize : array.length;
			results.add(_threadPool.submit(new PartitionProcessor(array, blockStart, blockEnd)));
		}
		
		// Collect results
		// Notice that the combination operation (the sum of the partial results) is performed sequentially
		try {
			int result = 0;
			for(Future<Long> res : results)
				result += res.get();
			return result;
		} 
		catch(InterruptedException | ExecutionException impossible)
		{
			// It won't happen!
			UnknownError error = new UnknownError();
			error.initCause(impossible);
			throw error;
		}
	}
	
	/**
	 * Adds the elements of the given array and produces the corresponding result.
	 * The implementation uses explicit synchronization to coordinate work completion
	 * and to ensure thread-safety while accessing shared state.
	 * 
	 * @param array The array that contains the values
	 * @return The result
	 */
	public static long addWithExplicitSynchronization(final long[] array)
	{
		// Define required synchronizers
		final AtomicLong result = new AtomicLong(0);
		final CountDownLatch latch = new CountDownLatch(PROCESSOR_COUNT);
		
		// Define computation work unit
		class PartitionProcessor implements Runnable
		{
			private final int _startIdx, _endIdx;
			private final long[] _array;
			
			public PartitionProcessor(long[] array, int start, int end)
			{
				_array = array;
				_startIdx = start;
				_endIdx = end;
			}
			
			@Override
			public void run()
			{
				int partialResult = 0;
				for(int idx = _startIdx; idx < _endIdx; ++idx)
					partialResult += _array[idx];
				
				result.addAndGet(partialResult);
				latch.countDown();
			}
		}
		
		// Assign work
		final int blockSize = array.length / PROCESSOR_COUNT;
		for(int partitionCount = 0; partitionCount < PROCESSOR_COUNT; ++partitionCount)
		{
			int blockStart = partitionCount * blockSize;
			int blockEnd = partitionCount != PROCESSOR_COUNT-1 ? blockStart + blockSize : array.length;
			_threadPool.submit(new PartitionProcessor(array, blockStart, blockEnd));
		}
		
		// Wait for completion and collect result
		// Notice that the combination operation (the sum of the partial results) was parallelized
		try {
			latch.await();
			return result.get();
		}
		catch(InterruptedException impossible)
		{
			// It won't happen!
			UnknownError error = new UnknownError();
			error.initCause(impossible);
			throw error;
		}
	}

	public static void forRange(final int startIdx, final int endIdx, final Action<Integer> body)
	{
		// Define required synchronizers
		final CountDownLatch latch = new CountDownLatch(PROCESSOR_COUNT);
		
		// Define computation work unit
		class PartitionProcessor implements Runnable
		{
			private final int _startIdx, _endIdx;
			private final Action<Integer> _forBody;
			
			public PartitionProcessor(int start, int end, Action<Integer> forBody)
			{
				_startIdx = start;
				_endIdx = end;
				_forBody = forBody;
			}
			
			@Override
			public void run()
			{
				Sequential.forRange(_startIdx, _endIdx, _forBody);
				latch.countDown();
			}
		}
		
		// Assign work
		final int workSize = (endIdx - startIdx);
		final int blockSize = workSize / PROCESSOR_COUNT;
		for(int partitionCount = 0; partitionCount < PROCESSOR_COUNT; ++partitionCount)
		{
			int blockStart = partitionCount * blockSize + startIdx;
			int blockEnd = partitionCount != PROCESSOR_COUNT-1 ? blockStart + blockSize : workSize;
			_threadPool.submit(new PartitionProcessor(blockStart, blockEnd, body));
		}
		
		// Wait for completion
		try {
			latch.await();
		}
		catch(InterruptedException impossible)
		{
			// It won't happen!
			UnknownError error = new UnknownError();
			error.initCause(impossible);
			throw error;
		}

	}
}
