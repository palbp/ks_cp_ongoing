package coriant.ks.cp.demos.forkjoin;

import coriant.ks.cp.utils.Functions.Action;

public final class ParallelFun {

	private static class ValueHolder
	{
		private volatile int _value = 0;
		public ValueHolder(int value) { _value = value; }
		public int get() { return _value; }
		public void set(int value) { _value = value; }
	}
		
	private static void testFor()
	{
		final int COUNT = (1 << 16); 
		final ValueHolder[] values = new ValueHolder[COUNT];

		for(int i=0; i<values.length; ++i)
			values[i] = new ValueHolder(i + 10);

		Action<Integer> body = new Action<Integer>() {
			@Override
			public void call(Integer idx)
			{
				int result = 0;
				for(int i = 1; i < idx; ++i)
					result += i % 2 == 0 ? i : -i;
					
				values[idx].set(values[idx].get() * (int) (Math.random() * 100) + result);
			}
		};
		
		final int REPETITION_COUNT = 100;
		long totalTime = 0; 
		for(int i=0; i<REPETITION_COUNT; ++i)
		{
			long startTime = System.nanoTime();
			Sequential.forRange(0, values.length, body);
			totalTime += (System.nanoTime() - startTime);
		}
		System.out.println("Sequential For = " + (totalTime / REPETITION_COUNT) / (1000 * 1000) + " ms");
		
		totalTime = 0; 
		for(int i=0; i<REPETITION_COUNT; ++i)
		{
			long startTime = System.nanoTime();
			TaskParallel.forRange(0, values.length, body);
			totalTime += (System.nanoTime() - startTime);
		}
		System.out.println("Parallel For (task) = " + (totalTime / REPETITION_COUNT) / (1000 * 1000) + " ms");

		totalTime = 0; 
		for(int i=0; i<REPETITION_COUNT; ++i)
		{
			long startTime = System.nanoTime();
			ThreadParallel.forRange(0, values.length, body);
			totalTime += (System.nanoTime() - startTime);
		}
		System.out.println("Parallel For = (thread) " + (totalTime / REPETITION_COUNT) / (1000 * 1000) + " ms");
	}
	
	public static void main(String[] args)
	{
		testFor();
	}
}
