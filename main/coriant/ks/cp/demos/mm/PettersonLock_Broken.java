package coriant.ks.cp.demos.mm;

public final class PettersonLock_Broken {

	private final boolean[] interested = new boolean[2];
	private int turn;
   
	public void lock(int i) 
	{
		interested[i] = true;
		turn = 1-i;

		while (turn == 1-i && interested[1-i]);
	}

	public void unlock(int i) 
	{
		interested[i] = false;
	}
	
	/**
	 * Shared state used in the thread-safety test.
	 */
	private static final PettersonLock_Broken _mutex = new PettersonLock_Broken();
	private static final int NTRIES = 10000000;
	private static int _sharedCount = 0;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws InterruptedException 
	{
		class TesterThread extends Thread 
		{
			private final int _tid;
			public TesterThread(int tid) { this._tid = tid; } 
			public void run() { 			
				for (int i = 0; i < NTRIES; ++i)
				{
					_mutex.lock(_tid);
					_sharedCount += 1;
					_mutex.unlock(_tid);
				}
			}
		};
		
		while(true)
		{
			long startTicks = System.currentTimeMillis();
			Thread t1 = new Thread(new TesterThread(0));
			Thread t2 = new Thread(new TesterThread(1));
	
			t1.start();
			t2.start();
		  
			t1.join();
			t2.join();
			
			System.out.printf("Expected = %d, Real=%d in %d ms!\n", 
					NTRIES*2, _sharedCount, System.currentTimeMillis()-startTicks);
		}
	}
}
