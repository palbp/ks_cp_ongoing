/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Implementation of a thread-safe counter.  
 *
 * A fix of the broken implementation of {@link CounterBroken}. 
 * The fix is based in the use of intrinsic locks to ensure visibility and atomicity.  
 */
public final class CounterFixLockFree 
{
	/**
	 * The number of items created so far.
	 */
	private final AtomicInteger _created = new AtomicInteger(0);
	
	/**
	 * The number of items destroyed so far. 
	 */
	private final AtomicInteger _destroyed = new AtomicInteger(0);
	
	/**
	 * Gets a relaxed estimation of the number of existing items.
	 * @return the approximate number of existing items.
	 */
	public int getCount()
	{
		return _created.get() - _destroyed.get(); 
	}
	
	/**
	 * Gets the number of created items.
	 * @return the number of created items.
	 */
	public int getCreatedCount()
	{
		return _created.get();
	}
	
	/**
	 * Gets the number of destroyed items.
	 * @return the number of destroyed items.
	 */
	public int getDestroyedCOunt()
	{
		return _destroyed.get();
	}
	
	/**
	 * Increments the number of existing items. 
	 */
	public void inc()
	{
		_created.incrementAndGet();
	}
	
	/**
	 * Decrements the number of existing items. 
	 */
	public void dec()
	{
		_destroyed.incrementAndGet();
	}
}
