/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm;

import java.util.concurrent.atomic.AtomicBoolean;
import coriant.ks.cp.utils.Functions.*;

/**
 * Class whose instances represent a future value associated to a given key. 
 * The value is computed on the first call to {@link get}.
 * 
 * The implementation is lock-free, but it uses spinning inappropriately.
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
public final class SimpleFutureLockFree<TKey,TValue> {
	
	/**
	 * The mapping function to be used.
	 */
	private final FuncNoError<TValue,TKey> _mapFunction; 

	/**
	 * Holds the pair's key
	 */
	private final TKey _key;
	
	/**
	 * Holds the pair's value or {@code null} if it has not yet been computed
	 */
	private volatile TValue _value;
	
	/**
	 * Flag that indicates whether the value is being computed. 
	 */
	private final AtomicBoolean _computing;
	
	/**
	 * Initiates an instance with the given key.
	 * 
	 * @param key The item's key
	 */
	public SimpleFutureLockFree(TKey key, FuncNoError<TValue,TKey> mapFunction)
	{
		_key = key;
		_value = null;
		_computing = new AtomicBoolean(false);
		_mapFunction = mapFunction;
	}
	
	/**
	 * Returns the item's value, computing it if necessary.
	 * 
	 * @return The item's value
	 */
	public TValue get()
	{
		if(_value != null)
			return _value;
		
		// Should the thread compute the future's value?
		if(!_computing.getAndSet(true))
		{
			// Flag was false, hence the thread won the race. Lets compute the value.
			_value = _mapFunction.call(_key);
			return _value;
		}
		
		// Some other thread is computing the future's value. Must wait for it.
		while(_value == null)
			;	// Spinning. In this case, is it a good idea? (discussion)
		return _value;
	}
}
