/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm;

import coriant.ks.cp.utils.Functions.*;

/**
 * Class whose instances represent a future value associated to a given key. 
 * The value is computed on the first call to {@link get}.
 * 
 * The implementation is based on the double-check locking pattern. 
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
public final class SimpleFutureDoubleCheckLock<TKey,TValue> {
	
	/**
	 * The mapping function to be used.
	 */
	private final FuncNoError<TValue,TKey> _mapFunction; 

	/**
	 * Holds the pair's key
	 */
	private final TKey _key;
	
	/**
	 * Holds the pair's value or {@code null} if it has not yet been computed
	 */
	private volatile TValue _value;
	
	/**
	 * Initiates an instance with the given key.
	 * 
	 * @param key The item's key
	 */
	public SimpleFutureDoubleCheckLock(TKey key, FuncNoError<TValue,TKey> mapFunction)
	{
		_key = key;
		_value = null;
		_mapFunction = mapFunction;
	}
	
	/**
	 * Returns the item's value, computing it if necessary.
	 * The intrinsic lock of this instance is used to block callers until the 
	 * item's value is computed.
	 * 
	 * @return The item's value
	 */
	public TValue get()
	{
		if(_value == null)
		{
			synchronized(this)
			{
				if(_value == null)
				{
					_value = _mapFunction.call(_key);
				}
			}
		}
		return _value;
	}
}
