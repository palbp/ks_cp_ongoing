/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm.ongoing;

import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicMarkableReference;

import coriant.ks.cp.demos.synchronizers.Latch;
import coriant.ks.cp.utils.Functions.*;

/**
 * Class whose instances represent a future value associated to a given key. 
 * The value is computed on the first call to {@link get}.
 * 
 * The implementation is lock-free, but it uses spinning inappropriately.
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
public final class FutureLockFree_8<TKey,TValue> {
	
	/**
	 * The mapping function to be used.
	 */
	private final FuncNoError<TValue,TKey> _mapFunction; 

	/**
	 * Holds the pair's key
	 */
	private final TKey _key;
	
	/**
	 * Holds the pair's value or {@code null} if it has not yet been computed
	 */
	private volatile TValue _value;
	
	/**
	 * Flag that indicates whether the value is being computed. 
	 */
	private final AtomicBoolean _computing;

	/**
	 * Holds the {@link Latch} instance to be used while waiting for the result, 
	 * or {@code null} if the instance has not been created. 
	 */
	private final AtomicMarkableReference<Latch> _latch;
	
	/**
	 * Initiates an instance with the given key.
	 * 
	 * @param key The item's key
	 */
	public FutureLockFree_8(TKey key, FuncNoError<TValue,TKey> mapFunction)
	{
		_key = key;
		_value = null;
		_computing = new AtomicBoolean(false);
		_latch = new AtomicMarkableReference<Latch>(null, false);
		_mapFunction = mapFunction;
	}
	
	/**
	 * Returns the item's value, computing it if necessary.
	 * The caller thread is blocked until item's value is computed, or the specified time has elapsed.
	 *
	 * @param timeout the time to wait (in milliseconds) 
	 * @return the item's value
	 * @throws InterruptedException if the calling thread was interrupted
	 * @throws TimeoutException if the specified time has elapsed
	 */
	public TValue get(long timeout) throws InterruptedException, TimeoutException
	{
		if(_value != null)
			return _value;
		
		// Should the thread compute the future's value?
		if(!_computing.getAndSet(true))
		{
			// Flag was false, hence the thread won the race. Lets compute the value.
			_value = _mapFunction.call(_key);
			
			// Wait for latch instantiation if in progress and signal blocked threads
			while(_latch.isMarked())
				; // Spin for a while
			
			Latch sync = _latch.getReference();
			if(sync != null)
				sync.open();
			
			return _value;
		}
		
		// Some other thread is computing the future's value. Must wait for it.
		// Racing for _latch instantiation
		if(_latch.compareAndSet(null, null, false, true))
		{
			// Note: Mark flag == true means that someone is in this path...
			
			// Race won. Lets instantiate the latch, but first...
			if(_value != null) 
			{
				// Value has become available in the meantime.
				while(!_latch.attemptMark(null, false));
				return _value;
			}
			
			_latch.set(new Latch(), false);
		}
		else {
			// Wait for latch instantiation 
			while(_latch.isMarked())
				; // Spin for a while
		}
		
		if(_value == null)
			// Block thread until value is available
			_latch.getReference().await(timeout);
		
		return _value;
	}
	
	/**
	 * Returns the item's value, computing it if necessary.
	 * The caller thread is blocked until item's value is computed.
	 * 
	 * @return the item's value
	 * @throws InterruptedException if the calling thread was interrupted
	 */
	public TValue get() throws InterruptedException
	{
		try {
			return get(0); // INFINITY = 0
		} catch (TimeoutException impossible) 
		{ 
			// It won't happen!
			UnknownError error = new UnknownError();
			error.initCause(impossible);
			throw error;
		}
	}
}
