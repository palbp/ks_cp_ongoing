/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm.ongoing;

public final class OneTimeProducerConsumer_0 {

	private static volatile boolean done = false;
	private static int value = 0;
	
	public static void main(String[] args) throws InterruptedException
	{
		Thread consumerT = new Thread() {
			public void run()
			{
				while(!done)
					;
				int v = value;
				System.out.println(v);
			}
		};
		
		Thread producerT = new Thread() {
			public void run()
			{
				try { 
					Thread.sleep(1000); 
					value = 42;
					done = true;
				} catch(InterruptedException ignored) { }
			}
		};
		
		consumerT.start();
		producerT.start();
		
		producerT.join();
		consumerT.join();
	}
}
