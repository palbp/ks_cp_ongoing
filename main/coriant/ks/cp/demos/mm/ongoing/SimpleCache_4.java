/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm.ongoing;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

import coriant.ks.cp.utils.Functions.*;

/**
 * A simplistic thread-safe cache implementation that stores (key,value) pairs. 
 * 
 * <ul>This version's properties:
 * 		<li> The value associated to a given key is computed exactly once, upon 
 * 		first retrieval </li>
 * 		<li> The implementation does not include a purge policy, meaning, it is 
 * 		unbounded </li> 
 * 		<li> {@code null} values and {@code null} keys are not allowed </li>
 * 		<li> The cache implementation is lock-free, 
 * </ul>
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
@SuppressWarnings("unused")
public final class SimpleCache_4<TKey,TValue> {

	/**
	 * Holds the cached (key,value) pairs. Stored values are @{link Future} instances. 
	 */
	
	private final ConcurrentMap<TKey,SimpleFutureDoubleCheckLock_5<TKey, TValue>> _cache;
	
	// TODO 
	// GOals: Illustrate the use of concurrent collections
	
	/**
	 * The mapping function to be used.
	 */
	private final FuncNoError<TValue,TKey> _mapFunction; 

	/**
	 * Initiates an instance with the provided key to value mapping function.
	 * 
	 * @param mapFunction The function that produces the value associated to a given key
	 */
	public SimpleCache_4(FuncNoError<TValue,TKey> mapFunction)
	{
		_mapFunction = mapFunction;
		_cache = new ConcurrentHashMap<>();
	}
	
	/**
	 * Returns the value associated to the given key, computing it if it has not yet been computed.
	 * If the value is not yet available, that is, it is being computed, the caller is blocked
	 * until the computation is completed.
	 * 
	 * @param key the key
	 * @return the value produced for the given key
	 */
	public TValue get(TKey key)
	{
		// Goals: 
		// Illustrate optimistic approach (do-verify) instead of a pessimistic one; 
		// The solution does not prevent multiple instantiation of the future.

		SimpleFutureDoubleCheckLock_5<TKey, TValue> value = _cache.get(key);
		if(value == null)
		{
			value = _cache.putIfAbsent(key, new SimpleFutureDoubleCheckLock_5<TKey, TValue>(key, _mapFunction));
		}
		
		return value.get();
	}
}
