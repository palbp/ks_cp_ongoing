/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm.ongoing;

/**
 * Implementation of an alleged thread-safe counter. 
 * 
 * Extracted from JBoss v4.0.5 implementation.
 * (an inner class of jboss.resource.connectionmanager.InternalManagedConnectionPool)
 * 
 * Where is the implementation broken?
 */
public final class CounterBroken_1 
{
	/**
	 * The number of items created so far.
	 */
	private int _created = 0;
	
	/**
	 * The number of items destroyed so far. 
	 */
	private int _destroyed;
	
	/**
	 * Gets a relaxed estimation of the number of existing items.
	 * @return the approximate number of existing items.
	 */
	public int getCount()
	{
		return _created - _destroyed; 
	}
	
	/**
	 * Gets the number of created items.
	 * @return the number of created items.
	 * 
	 * Broken!
	 */
	public int getCreatedCount()
	{
		return _created;
	}
	
	/**
	 * Gets the number of destroyed items.
	 * @return the number of destroyed items.
	 */
	public int getDestroyedCOunt()
	{
		return _destroyed;
	}
	
	/**
	 * Increments the number of existing items. 
	 */
	synchronized public void inc()
	{
		++_created;
	}
	
	/**
	 * Decrements the number of existing items. 
	 */
	synchronized public void dec()
	{
		++_destroyed;
	}
}
