package coriant.ks.cp.demos.mm.ongoing;

public final class Singleton {

	public static class Holder 
	{
		public final int x, y;
		
		public final static Holder _instance = new Holder();
		
		private Holder()
		{
			x = 42;
			y = x + 42;
		}
	}
	
	public static Holder getInstance()
	{
		return Holder._instance;
	}
	
	public static int GetZpto()
	{
		return 0;
	}
}
