/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm.ongoing;

import coriant.ks.cp.utils.Functions.*;

/**
 * Class whose instances represent a future value associated to a given key. 
 * The value is computed on the first call to {@link get}.
 * 
 * The implementation is based on the double-check locking pattern. 
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
public final class SimpleFutureDoubleCheckLock_5<TKey,TValue> {
	
	/**
	 * The mapping function to be used.
	 */
	private final FuncNoError<TValue,TKey> _mapFunction; 

	private final Object _monitor;
	private final TKey _key;
	private volatile TValue _value;
	
	/**
	 * Initiates an instance with the given key.
	 * 
	 * @param key The item's key
	 */
	public SimpleFutureDoubleCheckLock_5(TKey key, FuncNoError<TValue,TKey> mapFunction)
	{
		_mapFunction = mapFunction;
		_monitor = new Object();
		_key = key;
		_value = null;
	}
	
	/**
	 * Returns the item's value, computing it if necessary.
	 * The intrinsic lock of this instance is used to block callers until the 
	 * item's value is computed.
	 * 
	 * @return The item's value
	 */
	public TValue get()
	{
		// Goals: illustrate double-check locking pattern
		if(_value == null)
		{
			synchronized(_monitor)
			{
				if(_value == null)
					_value = _mapFunction.call(_key);
			}
		}
		return _value;
	}
}
