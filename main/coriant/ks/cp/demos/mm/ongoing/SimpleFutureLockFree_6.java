/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm.ongoing;

import coriant.ks.cp.utils.Functions.*;

/**
 * Class whose instances represent a future value associated to a given key. 
 * The value is computed on the first call to {@link get}.
 * 
 * The implementation is lock-free, but it uses spinning inappropriately.
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
@SuppressWarnings("unused")
public final class SimpleFutureLockFree_6<TKey,TValue> {
	
	/**
	 * The mapping function to be used.
	 */
	private final FuncNoError<TValue,TKey> _mapFunction; 

	// TODO
	
	/**
	 * Initiates an instance with the given key.
	 * 
	 * @param key The item's key
	 */
	public SimpleFutureLockFree_6(TKey key, FuncNoError<TValue,TKey> mapFunction)
	{
		_mapFunction = mapFunction;
		// TODO
	}
	
	/**
	 * Returns the item's value, computing it if necessary.
	 * 
	 * @return The item's value
	 */
	public TValue get()
	{
		// Goals: Wait for result through spinning. Discuss solution. 
		// TODO
		return null;
	}
}
