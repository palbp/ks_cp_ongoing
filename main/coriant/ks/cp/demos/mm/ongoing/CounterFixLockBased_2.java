/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm.ongoing;

/**
 * Implementation of a thread-safe counter.  
 *
 * A fix of the broken implementation of {@link CounterBroken_1}. 
 * The fix is based in the use of intrinsic locks.  
 */
public final class CounterFixLockBased_2 
{
	// TODO: use lock segregation
	
	/**
	 * The number of items created so far.
	 */
	private int _created = 0;
	
	/**
	 * The number of items destroyed so far. 
	 */
	private int _destroyed;
	
	/**
	 * Gets the number of existing items.
	 * @return the number of existing items.
	 */
	synchronized public int getCount()
	{
		return _created - _destroyed; 
	}
	
	/**
	 * Gets the number of created items.
	 * @return the number of created items.
	 */
	synchronized public int getCreatedCount()
	{
		return _created;
	}
	
	/**
	 * Gets the number of destroyed items.
	 * @return the number of destroyed items.
	 */
	synchronized public int getDestroyedCOunt()
	{
		return _destroyed;
	}
	
	/**
	 * Increments the number of existing items. 
	 */
	synchronized public void inc()
	{
		++_created;
	}
	
	/**
	 * Decrements the number of existing items. 
	 */
	synchronized public void dec()
	{
		++_destroyed;
	}
}
