/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 3.
 * February 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.mm;

import java.util.concurrent.*;

import coriant.ks.cp.utils.Functions.*;

/**
 * A simplistic thread-safe cache implementation that stores (key,value) pairs. 
 * 
 * <ul>This version's properties:
 * 		<li> The value associated to a given key is computed exactly once, upon 
 * 		first retrieval </li>
 * 		<li> The implementation does not include a purge policy, meaning, it is 
 * 		unbounded </li> 
 * 		<li> {@code null} values and {@code null} keys are not allowed </li>
 * 		<li> The cache implementation is lock-free, 
 * </ul>
 *
 * @param <TKey> The pair's key type
 * @param <TValue> The pair's value type
 */
public final class SimpleCache<TKey,TValue> {

	/**
	 * Holds the cached (key,value) pairs. Stored values are @{link Future} instances. 
	 */
	private final ConcurrentMap<TKey,SimpleFutureDoubleCheckLock<TKey,TValue>> _cache;
	
	/**
	 * The mapping function to be used.
	 */
	private final FuncNoError<TValue,TKey> _mapFunction; 

	/**
	 * Initiates an instance with the provided key to value mapping function.
	 * 
	 * @param mapFunction The function that produces the value associated to a given key
	 */
	public SimpleCache(FuncNoError<TValue,TKey> mapFunction)
	{
		_mapFunction = mapFunction;
		_cache = new ConcurrentHashMap<TKey,SimpleFutureDoubleCheckLock<TKey,TValue>>();
	}
	
	/**
	 * Returns the value associated to the given key, computing it if it has not yet been computed.
	 * If the value is not yet available, that is, it is being computed, the caller is blocked
	 * until the computation is completed.
	 * 
	 * @param key the key
	 * @return the value produced for the given key
	 */
	public TValue get(TKey key)
	{
		SimpleFutureDoubleCheckLock<TKey,TValue> futureValue = _cache.get(key);
		if(futureValue == null)
		{
			// Future instance does not yet exist.
			// Racing for its creation!
			futureValue = new SimpleFutureDoubleCheckLock<TKey,TValue>(key, _mapFunction);
			SimpleFutureDoubleCheckLock<TKey,TValue> existing = _cache.putIfAbsent(key, futureValue);
			
			// Did we win the race?
			if(existing != null)
			{
				// We lost. Discard the future we instantiated and use the existing one.
				futureValue = existing;
			}
		}
		return futureValue.get();
	}
}
