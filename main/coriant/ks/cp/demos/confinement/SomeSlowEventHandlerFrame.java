/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos to be used in Sessions 1, 2, and 3.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.confinement;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.beans.*;
import java.util.concurrent.*;

@SuppressWarnings("serial")
public final class SomeSlowEventHandlerFrame extends JFrame {

	/**
	 * UI controls
	 */
	private final JLabel _message;
	private final JProgressBar _progress;
	private final JButton _start, _cancel;
	
	/**
	 * Text pertaining to the presented messages. 
	 */
	private static final String IDLE_MSG = "Idle", WORK_MSG = "Working...", DONE_MSG = "Done!";
	
	/**
	 * Future instance that represents the background computation work. 
	 */
	private Future<String> _work;

	/**
	 * Constant used to switch the background worker implementation in use. 
	 */
	private static final boolean ENABLE_MY_BW = true;

	/**
	 * Starts the background work using an instance of {@link BackgroundWorker}
	 */
	private void startMyWorker()
	{
		BackgroundWorker<String> worker = new BackgroundWorker<String>() {
			@Override
			public String doInBackground() throws Exception
			{
				// Doing something slow...
				for(int i=0; i<20; ++i)
				{
					if(isCancelled())
						return null;
					setProgress(i*5);
					Thread.sleep(500); 
				}
				setProgress(100);
				return DONE_MSG;
			}
			
			@Override
			public void done()
			{
				// Completed
				try { 
					String result = get(); 
					_message.setText(result);
				} 
				catch (CancellationException e) {
					setProgress(0);
					_message.setText(IDLE_MSG);
				} 
				catch (ExecutionException | InterruptedException ignored) { }  
				finally {
					_start.setEnabled(true);
					_cancel.setEnabled(false);
				}
			}

			@Override
			protected void reportProgress(int percentage)
			{
				_progress.setValue(percentage);
			}
		};
		_work = worker;
		worker.execute();
	}
	
	/**
	 * Starts the background work using an instance of {@link SwingWorker}
	 */
	private void startSwingWorker()
	{
		SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
			
			{
				addPropertyChangeListener(new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent evt)
					{
			             if ("progress".equals(evt.getPropertyName())) {
			                 _progress.setValue((Integer)evt.getNewValue());
			             }
					}
				});
			}
			
			@Override
			public String doInBackground() throws Exception
			{
				// Doing something slow...
				for(int i=0; i<20; ++i)
				{
					if(isCancelled())
						return null;
					setProgress(i*5);
					Thread.sleep(500); 
				}
				setProgress(100);
				return DONE_MSG;
			}
			
			@Override
			public void done()
			{
				// Completed
				try { 
					String result = get(); 
					_message.setText(result);
				} 
				catch (CancellationException e) {
					setProgress(0);
					_message.setText(IDLE_MSG);
				} 
				catch (Exception ignored) { } 
				finally {
					_start.setEnabled(true);
					_cancel.setEnabled(false);
				}
			}
		};
		_work = worker;
		worker.execute();
		
	}
	
	/**
	 * Sets the frame's behavior (i.e. reactions to UI events)
	 */
	private void initBehavior()
	{
		_start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				// Setup
				_start.setEnabled(false);
				_cancel.setEnabled(true);
				_progress.setValue(0);
				_message.setText(WORK_MSG);
			
				// Schedule work
				if(ENABLE_MY_BW)
					startMyWorker();
				else
					startSwingWorker();
			}
		});
		
		_cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				_work.cancel(true);
			}
		});
	}
	
	/**
	 * Initiates an instance.
	 */
	public SomeSlowEventHandlerFrame()
	{
		super("Slow handler demo");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(_message = new JLabel(IDLE_MSG, JLabel.CENTER), BorderLayout.NORTH);
		add(_progress = new JProgressBar(), BorderLayout.CENTER);
		final JPanel southPanel = new JPanel();
		southPanel.add(_start = new JButton("Start")); 
		southPanel.add(_cancel = new JButton("Cancel")); 
		_cancel.setEnabled(false);
		add(southPanel, BorderLayout.SOUTH);
		pack();
		setSize(getWidth()*2, getHeight());
	}
	
	/**
	 * Shows the current instance and enables it's behavior.
	 * (note: designed to prevent "this escape")
	 */
	public void showWindow()
	{
		if(isVisible()) return;
		initBehavior();
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(); 
		setLocation((screen.width-getWidth())/2, (screen.height-getHeight())/2);
		setVisible(true);
	}
	
	/**
	 * Application entry point
	 * 
	 * @param args
	 */
	public static void main(String[] args) 
	{
		new SomeSlowEventHandlerFrame().showWindow();
	}
}
