/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos to be used in Session 4.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.confinement;

import java.awt.EventQueue;
import java.util.concurrent.*;

/**
 * Class that implements a background worker. The class design mandates that each instance is only used once. 
 *  
 * The class provides a pull interface, meaning, the data regarding the computation
 * is pulled by calling the {@link get} method. 
 * 
 * <ul>This version's properties:
 * 		<li> Background work may be canceled</li>
 * 		<li> Background work progress reported by push style interface</li>
 * 		<li> Computation status may be accessed while in progress</li>
 * </ul>
 * 
 * @author Paulo Pereira
 */
public abstract class BackgroundWorker<Result> implements Future<Result>
{
	/**
	 * The number of processors of the target system.
	 */
	private static final int PROCESSOR_COUNT = Runtime.getRuntime().availableProcessors();
	
	/**
	 * The time (in milliseconds) that extra threads (those that exceed the minimum amount) may remain idle
	 * before they are discarded.
	 */
	private static final int TIMEOUT = 200;
	
	/** 
	 * The thread pool executor used to run background work.
	 */
	private static Executor _workerPool = new  ThreadPoolExecutor(PROCESSOR_COUNT , PROCESSOR_COUNT, 
			TIMEOUT, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<Runnable>());  
	
	/**
	 * A FutureTask implementation that performs the background work and the required
	 * synchronization.
	 */
	private final FutureTask<Result> _result = 
			new FutureTask<Result>(
					new Callable<Result>() {
						@Override
						public Result call() throws Exception { return doInBackground(); }
					}) {
		
				@Override
				protected void done()
				{
					EventQueue.invokeLater(new Runnable() {
						@Override
						public void run() { BackgroundWorker.this.done(); }
					});
				}
			};

	/**
	 * Triggers the background computation defined by the {@link doInBackground} method. 
	 */
	public final void execute()
	{
		// Unbounded thread creation: not a good idea!
		// new Thread(_result).start();
		
		// Lets do it right, by using an Executor
		_workerPool.execute(_result);
	}
	
	/**
	 * Waits if necessary for the computation to complete, and then retrieves its result.
	 *  
	 * @return the computed result
	 * @throws ExecutionException if the computation fails
	 * @throws InterruptedException if the computation was interrupted
	 */
	@Override
	public final Result get() throws ExecutionException, InterruptedException
	{
		return _result.get();
	}
	
	/**
	 * Waits if necessary for the computation to complete within the specified 
	 * time interval, and then retrieves its result.
	 *  
	 * @return the computed result
	 * @throws ExecutionException if the computation fails
	 * @throws InterruptedException if the computation was interrupted
	 * @throws TimeoutException if the computation does not complete in the given time interval 
	 */
	@Override
	public final Result get(long timeout, TimeUnit unit) throws ExecutionException, 
		InterruptedException, TimeoutException
	{
		return _result.get(timeout, unit);
	}

	/**
	 * Attempts to cancel the background computation. 
	 * This attempt will fail if the computation has already completed, has already been cancelled, 
	 * or could not be cancelled for some other reason. 
	 * If successful, and the computation has not started when cancel is called, it should never run. 
	 * If the computation has already started, then the mayInterruptIfRunning parameter determines whether 
	 * the thread executing the computation should be interrupted in an attempt to stop it.
	 * 
	 * @param mayInterruptIfRunning  if the thread executing the computation should be interrupted; otherwise, 
	 * in-progress computations are allowed to complete
	 * @return {@code false} if the computation could not be cancelled; {@code true} otherwise
	 */
	@Override
	public boolean cancel(boolean mayInterruptIfRunning)
	{
		return _result.cancel(mayInterruptIfRunning);
	}

	/**
	 * Returns {code true} if the computation was cancelled before it completed normally. 
	 * 
	 * @return {@code true} if the computation was cancelled before it completed
	 */
	@Override
	public boolean isCancelled()
	{
		return _result.isCancelled();
	}

	/**
	 * Returns {@code true} if the computation is completed. Completion may be due to normal termination 
	 * (i.e. successfully or throwing an exception), or cancellation.
	 * 
	 *  @return {@code true} if the computation is completed.
	 */
	@Override
	public boolean isDone()
	{
		return _result.isDone();
	}
	
	/**
	 * Called to publish progress changes. 
	 * 
	 * @param progress The current progress, in percentage of completed work 
	 * @throws IllegalArgumentException if {@link progress} is not within the 
	 * specified interval (i.e. [0..100] 
	 */
	protected final void setProgress(final int progress)
	{
		if(progress < 0 || progress > 100)
			throw new IllegalArgumentException();

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() { reportProgress(progress); }
		});
	}
	
	/**
	 * Executes in a background thread. This method must be overridden in order to define the computation
	 * to be performed in background. 
	 * 
	 * @return the computed result
	 * @throws Exception if an error occurred while performing the computation
	 */
	protected abstract Result doInBackground() throws Exception;
	
	/**
	 * Executes in the Event Dispatcher thread. Called to notify computation progress changes.
	 *  
	 * @param progress The current percentage of completed work
	 */
	protected abstract void reportProgress(int progress);

	/**
	 * Executes in the Event Dispatcher thread. Called upon computation completion.
	 *  
	 * @param result The computation's result
	 */
	protected abstract void done();
}
