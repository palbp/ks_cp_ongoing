/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 2.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.synchronizers;

import java.util.concurrent.TimeoutException;

/**
 * Implementation of a synchronizer functionally equivalent to the one provided in
 * {@link java.util.concurrent.Exchanger<T>}
 * 
 * For a detailed explanation of an implementation that supports high contention, refer to
 * {@link http://grepcode.com/file/repository.grepcode.com/java/root/jdk/openjdk/7-b147/java/util/concurrent/Exchanger.java}
 */
public final class Exchanger<T> {

	/**
	 * Class whose instances represent blocked threads' requests.
	 */
	private static final class Request<T>
	{
		public boolean _serviced;
		public T _message;
		
		public Request(T ownMessage) 
		{ 
			_serviced = false; 
			_message = ownMessage; 
		}
		
		public T trade(T othersMessage) 
		{ 
			T myMessage = _message;
			_message = othersMessage;
			_serviced = true; 
			return myMessage;
		}
	}
	
	/**
	 * Private monitor used to guard accesses to shared data. 
	 */
	private final Object _monitor;
	
	/**
	 * Shared state that holds the blocked thread's request. 
	 * Guarded by {@code _monitor}. 
	 * Notice that there is, at most, one blocked thread. 
	 */
	private Request<T> _request; 
	
	/**
	 * Initiates an instance.
	 */
	public Exchanger()
	{
		_request = null;
		_monitor = new Object();
	}
	
	/**
	 * Waits for another thread to arrive at this message exchange point. 
	 *  
	 * @param myMessage the object to exchange
	 * @return the received object
	 * @throws InterruptedException if the thread was canceled
	 */
	public T exchange(T myMessage) throws InterruptedException
	{
		try {
			return exchange(myMessage, 0 /*INFINITY*/);
		}
		catch(TimeoutException _impossible)
		{
			throw new AssertionError();
		}
	}
	
	
	/**
	 * Waits for another thread to arrive at this message exchange point. 
	 * 
	 * @param myMessage the object to exchange
	 * @param timeout the time to wait (in milliseconds)
	 * @return the received object
	 * @throws TimeoutException if the specified timeout was exceeded
	 * @throws InterruptedException if the thread was canceled
	 */
	public T exchange(T myMessage, long timeout) throws TimeoutException, InterruptedException 
	{
		synchronized(_monitor)
		{
			if(_request != null)
			{
				// We may proceed! Exchange message with the waiting thread
				T rcvdMessage = _request.trade(myMessage);
				_request = null;
				_monitor.notify();
				return rcvdMessage;
			}
			
			// Get a temporal reference
			long waitStart = (timeout != 0 /*INFINITY*/) ? System.currentTimeMillis() : 0 /*INFINITY*/;

			// Publish request to notifiers
			Request<T> ownRequest = new Request<T>(myMessage);
			_request = ownRequest;
			
			try {
				while(true)
				{
					_monitor.wait(timeout);

					// Check if thread was signaled
					if(ownRequest._serviced)
						return ownRequest._message;

					// Check for timeout (if not INFINITY)
					if(timeout != 0 /*INFINITY*/)
					{
						long now = System.currentTimeMillis();
						long elapsed = now - waitStart;
						
						if(elapsed >= timeout)
						{
							// Thread gave up. Must remove request.
							_request = null;
							throw new TimeoutException();
						}
						
						timeout -= elapsed;
						waitStart = now;
					}
				}
			}
			catch(InterruptedException ie)
			{
				// Must clean up. If a request exists, it is surely this thread's
				_request = null;
				throw ie;
			}
		}
	}
}
