/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 2.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.synchronizers;

import java.util.LinkedList;
import java.util.List;

/**
 * Class that implements a gate synchronizer. The PI suffix stands for Plain
 * Implementation.
 * 
 * A latch is initiated in the non-signaled state (i.e. closed), meaning that calls  
 * to await block the calling threads. Threads are unblocked when the synchronizer 
 * transitions to the signaled state (i.e. open). Once open, latches cannot be
 * closed. 
 * 
 * This synchronizer is a variant of the {@link java.util.concurrent.CyclicBarrier}
 * synchronizer, where the number of participants is undetermined. In these cases, at 
 * least one of the participants must have a distinct role: it must explicitly signal 
 * blocked threads by calling open, instead of implicitly signaling blocked participants 
 * by being the last one to arrive at the blocking operation (i.e. await).
 * 
 * Implementation note:
 * Although correct, the current implementation does not include any optimization,
 * although some optimizations could be easily attained. 
 * The implementation's main purpose is therefore to provide a clear notion of 
 * how to use the delegated execution technique.
 */
public final class GatePI {

	/**
	 * Class whose instances represent blocked threads' requests
	 */
	private class Request {
		/**
		 * Indicates whether the owning thread has been signaled. 
		 */
		public boolean _isSignaled = false;
	}
	
	/**
	 * Shared state indicating whether the gate is open or not.
	 * @Guarded by {@code _requests}'s intrinsic monitor 
	 */
	private boolean _open;
	
	/**
	 * Shared state that holds blocked threads's requests
	 * @Guarded by {@code _requests}'s intrinsic monitor 
	 */
	private final List<Request> _requests; 
	
	/**
	 * Creates an instance. The gate is initially closed.
	 */
	public GatePI()
	{
		_open = false;
		_requests = new LinkedList<>();
	}
	
	/**
	 * Opens the gate.
	 */
	public void open()
	{
		synchronized(_requests)
		{
			_open = true;
			for(Request req : _requests)
				req._isSignaled = true;
			_requests.clear();
			_requests.notifyAll();
		}
	}
	
	/**
	 * Closes the gate.
	 */
	public void close()
	{
		synchronized(_requests)
		{
			_open = false;
		}
	}
	
	/**
	 * The caller thread is blocked until the gate is opened.
	 *  
	 * @throws InterruptedException if the thread was interrupted
	 */
	public void await() throws InterruptedException
	{
		synchronized(_requests)
		{
			if(_open)
				return;
			
			Request req = new Request();
			_requests.add(req);
			try {
				while(true)
				{
					_requests.wait();
					
					if(req._isSignaled)
						return;
				}
			}
			catch(InterruptedException ie)
			{
				_requests.remove(req);
				throw ie;
			}
		}
	}
}
