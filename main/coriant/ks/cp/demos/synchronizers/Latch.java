/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 2.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.synchronizers;

/**
 * Class that implements a latch synchronizer.
 * 
 * A latch is initiated in the non-signaled state (i.e. closed), meaning that calls  
 * to await block the calling threads. Threads are unblocked when the synchronizer 
 * transitions to the signaled state (i.e. open). Once open, latches cannot be
 * closed. 
 * 
 * This synchronizer is functionally equivalent to {@link java.util.concurrent.CountDownLatch}
 * instances initiated with one unit.
 */
public final class Latch {

	/**
	 * Private monitor used to guard access to the synchronizer's shared state.
	 */
	private final Object _monitor;
	
	/**
	 * Shared state indicating whether the latch is open or not.
	 */
	private boolean _open;
	
	/**
	 * Creates an instance. The latch is initially closed.
	 */
	public Latch()
	{
		_monitor = new Object();
		_open = false;
	}
	
	/**
	 * The caller thread is blocked until the latch is opened, the specified
	 * timeout has elapsed, or the executing thread is cancelled (a.k.a. interrupted).
	 * 
	 * @param timeout the time to wait (in milliseconds) 
	 * @return {@code true} if the thread was signaled, {@code false} if specified 
	 * timeout has elapsed
	 * @throws InterruptedException if the calling thread was interrupted
	 */
	public boolean await(long timeout) throws InterruptedException
	{
		synchronized(_monitor)
		{
			if(_open)
				return true;
			
			// Get a temporal reference (INFINITY == 0)
			long waitStart = (timeout != 0) ? System.currentTimeMillis() : 0;
			
			while(true)
			{
				_monitor.wait(timeout);
				
				if(_open)
					return true;
				
				// Check for timeout (if not INFINITY)
				if(timeout != 0)
				{
					long now = System.currentTimeMillis();
					long elapsed = now - waitStart;
					
					if(elapsed >= timeout) // Thread gave up. 
						return false; 
					
					timeout -= elapsed;
					waitStart = now;
				}
			}
		}
	}
	
	/**
	 * The caller thread is blocked until the latch is opened or the executing 
	 * thread is cancelled (a.k.a. interrupted).
	 *  
	 * @throws InterruptedException if the thread was interrupted
	 */
	public void await() throws InterruptedException
	{
		await(0); // INFINITY = 0
	}
	
	/**
	 * Opens the latch, thus signaling blocked threads.
	 */
	public void open()
	{
		synchronized(_monitor)
		{
			_open = true;
			_monitor.notifyAll();
		}
	}
}
