/**
 * Code base for the Concurrent Programming in Java Knowledge Sharing sessions.
 * 
 * Demos used in Session 2.
 * January 2014 
 * 
 * @author Paulo Pereira
 */
package coriant.ks.cp.demos.synchronizers;

/**
 * Class that implements a gate synchronizer.
 * 
 * A latch is initiated in the non-signaled state (i.e. closed), meaning that calls  
 * to await block the calling threads. Threads are unblocked when the synchronizer 
 * transitions to the signaled state (i.e. open). Once open, latches cannot be
 * closed. 
 * 
 * This synchronizer is a variant of the {@link java.util.concurrent.CyclicBarrier}
 * synchronizer, where the number of participants is undetermined. In these cases, at 
 * least one of the participants must have a distinct role: it must explicitly signal 
 * blocked threads by calling open, instead of implicitly signaling blocked participants 
 * by being the last one to arrive at the blocking operation (i.e. await).
 * 
 * Implementation notes:
 * The current implementation, as opposed to GatePI, makes use of a single node that 
 * is shared between blocked threads, thus reducing space overhead and reducing the 
 * time spent on the notification path. Further optimizations would require the use of 
 * more elaborate techniques.
 * For the sake of simplification, the current implementation does not support timed waits.
 */
public final class Gate {

	/**
	 * Class whose instances represent blocked threads' requests
	 */
	private class Request {
		/**
		 * Indicates whether the owning thread has been signaled. 
		 */
		public boolean _isSignaled = false;
		
		/**
		 * Indicates the number of threads that share the current instance.
		 */
		public short _waitingCount = 0;
	}
	
	/**
	 * Shared state indicating whether the gate is open or not.
	 * @Guarded by {@code _monitor}'s intrinsic monitor 
	 */
	private boolean _open;
	
	/**
	 * Shared state that holds the blocked threads's shared request
	 * @Guarded by {@code _monitor}'s intrinsic monitor 
	 */
	private Request _sharedRequest; 
	
	/**
	 * Holds the private monitor used to guard access to shared state.
	 */
	private final Object _monitor;
	
	/**
	 * Creates an instance. The gate is initially closed.
	 */
	public Gate()
	{
		_open = false;
		_monitor = new Object();
		_sharedRequest = null;
	}
	
	/**
	 * Opens the gate
	 */
	public void open()
	{
		synchronized(_monitor)
		{
			_open = true;
			if(_sharedRequest != null)
			{
				_sharedRequest._isSignaled = true;
				_sharedRequest = null;
				_monitor.notifyAll();
			}
		}
	}
	
	/**
	 * Closes the gate
	 */
	public void close()
	{
		synchronized(_monitor)
		{
			_open = false;
		}
	}
	
	/**
	 * The caller thread is blocked until the gate is opened.
	 *  
	 * @throws InterruptedException if the thread was interrupted
	 */
	public void await() throws InterruptedException
	{
		synchronized(_monitor)
		{
			if(_open)
				return;
			
			Request req = _sharedRequest == null ? _sharedRequest = new Request() : _sharedRequest;
			req._waitingCount += 1;
			
			try {
				while(true)
				{
					_monitor.wait();
					
					if(req._isSignaled)
						return;
				}
			}
			catch(InterruptedException ie)
			{
				// The thread was cancelled. Let's clean-up.
				if(--req._waitingCount == 0)
					_sharedRequest = null;
				throw ie;
			}
		}
	}
}
