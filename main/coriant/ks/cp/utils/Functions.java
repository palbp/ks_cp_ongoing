package coriant.ks.cp.utils;

/**
 * Class that holds function utilities. To be used in environments prior to Java 8.
 * 
 * @author Paulo Pereira
 */
public final class Functions 
{
	/**
	 * Interface to be implemented by functions that receive a parameter and produce a result. 
	 *
	 * @param <TResult> the result type
	 * @param <TParam>  the parameter type
	 */
	public static interface Func<TResult, TParam> 
	{
		/**
		 * Executes the function.
		 * 
		 * @param param the action's parameter
		 * @return the action's result
		 * @throws Exception the action's error, if one occurred
		 */
		public TResult call(TParam param) throws Exception;
	}
	
	/**
	 * Interface to be implemented by functions that receive a parameter and produce a result.
	 * The function's execution does not produce an error, hence the absence of a throws clause.
	 *
	 * @param <TResult> the result type
	 * @param <TParam>  the parameter type
	 */
	public static interface FuncNoError<TResult, TParam> 
	{
		/**
		 * Executes the function.
		 * 
		 * @param param the action's parameter
		 * @return the action's result
		 */
		public TResult call(TParam param);
	}
	
	/**
	 * Interface to be implemented by actions that receive a parameter and produce no result. 
	 *
	 * @param <TParam>  the parameter type
	 */
	public static interface Action<TParam> 
	{
		/**
		 * Executes the action.
		 * 
		 * @param param the action's parameter
		 */
		public void call(TParam param);
	}

	/**
	 * Interface to be implemented by functions that aggregates values. 
	 *
	 * @param <T> the type of the elements to aggregate
	 */
	public static interface Aggregate<T> {
		/**
		 * Executes the function.
		 * 
		 * @param aggregated the current aggregated value
		 * @param current the value to be aggregated
		 * @return the resulting aggregated value
		 */
		public T call(T aggregated, T current);
	}
}
